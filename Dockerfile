# Сборка образа komin

FROM openjdk:8-alpine
COPY ./target/komin-*.jar app.jar
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["sh", "/entrypoint.sh"]