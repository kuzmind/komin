#how to start the project?
#1) Start Kurento docker container by using these commands:
 
#### $ docker pull kurento/kurento-media-server:latest
 
#### $ docker run --name kms -d -p 8888:8888 kurento/kurento-media-server:latest
#2) Invoke maven install command
#3) Start spring boot application
#4) Or instead of steps 2 and 3 you can run ./mvnw script from the root of the project
### ./mvnw spring-boot:run   


#start docker compose

./mvnw clean install spring-boot:repackage ; docker-compose up -d --build

#### Useful docker commands

```
docker-compose up -d  start services without build if image exists 
docker-compose down - down services in current docker compose
docker container ls - print running containers
docker logs 4036b9282835 - view container log (even it is down)
```
