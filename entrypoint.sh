#!/bin/bash

echo "JVM_OPTS=${JVM_OPTS}"
echo "java ${JVM_OPTS} -Dspring.profiles.active=container -jar /app.jar"
java $JVM_OPTS -Dspring.profiles.active=container -jar /app.jar