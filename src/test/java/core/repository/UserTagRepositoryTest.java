package core.repository;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.komin.core.model.UserSession;
import com.komin.core.entities.TagImpl;
import com.komin.core.repository.UserTagRepository;
import com.komin.core.repository.UserTagRepositoryImpl;
import com.komin.core.repository.UserRepository;
import com.komin.core.services.UserTagsInfoService;
import com.komin.core.utils.ResolvingTagsFromUserUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.HttpSession;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.komin.core.ApplicationConstants.MOST_POPULAR_TAGS_COUNT;
import static com.komin.core.JsonConstants.COUNT;

@RunWith(SpringRunner.class)
public class UserTagRepositoryTest {

    @TestConfiguration
    public static class TagRepositoryTestContextConfiguration {

        @Bean
        public UserRepository userRepository() {
            return new UserRepository();
        }

        @Bean
        public UserTagRepository tagRepository() {
            return new UserTagRepositoryImpl();
        }

        @Bean
        public UserTagsInfoService tagsInfoService() { return new UserTagsInfoService(userRepository(), tagRepository());}
    }

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserTagRepository userTagRepository;
    @Autowired
    private UserTagsInfoService userTagsInfoService;

    private final List<UserSession> userSessions = new ArrayList<>();
    private final Map<String, List<String>> stringSetMap = new ConcurrentHashMap<>();
    private final Set<String> strings = new HashSet<>();
    private final String[] tags = new String[] {"Russia", "China", "USA", "Work", "Family", "Girls", "Boys", "Date", "Friends",
            "Spring", "Service", "Test", "Controller", "Unit", "During", "component", "scanning", "we" ,"might" , "find",
            "components", "or", "configurations", "created" , "only" ,"for" ,"specific", "tests" ,"accidentally"};

    @Before
    public void prepare() {

        HttpSession httpSessionMock = Mockito.mock(HttpSession.class);

        for (int i = 0; i < 10; i++) {
            List<String> taggs = new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                Random random = new Random();
                int nextInt = random.nextInt(tags.length);
                String tag = tags[nextInt].toLowerCase();
                taggs.add(tag);
                strings.add(tag);
            }
            stringSetMap.put(String.valueOf(i), taggs);
        }

        for (int i = 0; i < 10; i++) {
            UserSession userSession = new UserSession(httpSessionMock, String.valueOf(i));
            userRepository.register(userSession);
            userSession.initTags(stringSetMap.get(String.valueOf(i)));
            userSessions.add(userSession);
        }

        userSessions.forEach(user -> userTagRepository.registerTags(user));
    }

    @After
    public void clean() {
        userSessions.clear();
        userTagRepository.updateInfo(Collections.EMPTY_LIST);
    }

    @Test
    public void infoAboutTagsServiceTest() {
        JsonObject tagsInfo = userTagsInfoService.getTagsInfo();
        Assert.assertNotNull(tagsInfo);

        Map<String, Set<String>> tagsMap = userTagRepository.getTagsMap();
        Random random = new Random();
        int i = random.nextInt(tagsInfo.size());
        int j = 0;
        int resultCount = 0;
        String resultName = "";
        for (Map.Entry<String, Set<String>> entry : tagsMap.entrySet()) {
            if (j == i) {
                resultCount = entry.getValue().size();
                resultName = entry.getKey();
                break;
            }
            j++;
        }

        JsonObject tagInfo = userTagsInfoService.getTagInfo(resultName);
        JsonElement count = tagInfo.get(COUNT);
        Assert.assertEquals(resultCount, count.getAsInt());
    }

    @Test
    public void tagsCrudTest() {
        Map<String, Set<String>> tagsMap = userTagRepository.getTagsMap();
        Assert.assertEquals(strings.size(), tagsMap.size());
        userSessions.forEach(user -> userTagRepository.deleteTags(user));
        Assert.assertEquals(0, tagsMap.size());
    }

    @Test
    public void mostPopularTagsTest() {
        ArrayList<Integer> resultSet1 = getMostPopularTags();
        ArrayList<Integer> resultSet2 = new ArrayList<>();
        ((UserTagRepositoryImpl) userTagRepository).sortTags();
        Collection<TagImpl> mostPopularTags = userTagRepository.getMostPopularTags();
        for (TagImpl tag : mostPopularTags) {
            resultSet2.add(tag.getCount());
        }
        boolean deepEquals = Objects.deepEquals(resultSet1, resultSet2);
        Assert.assertTrue(deepEquals);
    }

    private ArrayList<Integer> getMostPopularTags() {
        Map<String, Set<String>> tagsMap = userTagRepository.getTagsMap();
        int size = tagsMap.size();
        int max = 0;
        Set<String> maxGuys = new HashSet<>();
        ArrayDeque<String> result = new ArrayDeque<>();
        String maxCandidate = "";
        for (int i = 0; i < size; i++) {
            for (Map.Entry<String, Set<String>> entry : tagsMap.entrySet()) {
                String tag = entry.getKey();
                Set<String> strings = entry.getValue();
                if (strings.size() > max && !maxGuys.contains(tag)) {
                    max = strings.size();
                    maxCandidate = tag;
                }
            }
            maxGuys.add(maxCandidate);
            result.addLast(maxCandidate);
            max = 0;
            maxCandidate = "";
        }
        ArrayList<Integer> resultSet1 = new ArrayList<>();
        for (int i = 0; i < MOST_POPULAR_TAGS_COUNT && !result.isEmpty(); i++) {
            String s = result.pollFirst();
            resultSet1.add(tagsMap.get(s).size());
        }
        resultSet1.sort(Comparator.reverseOrder());
        return resultSet1;
    }

    @Test
    public void mostPopularTagsAfterDeleteTest() {
        int size = userSessions.size();
        Random random = new Random();
        for (int i = 0; i < size / 2; i++) {
            int i1 = random.nextInt(size);
            UserSession userSession = userSessions.get(i1);
            userTagRepository.deleteTags(userSession);
        }
        mostPopularTagsTest();
    }

    @Test
    public void resolvingTagsUtilTest() {
        int size = stringSetMap.size();
        Random random = new Random();
        int i = random.nextInt(size);
        String sessionId = String.valueOf(i);
        List<String> strings = stringSetMap.get(sessionId);
        UserSession resolve = ResolvingTagsFromUserUtils.resolve(userSessions, strings);
        UserSession rightUser = userRepository.getBySessionId(sessionId);
        Assert.assertEquals(rightUser, resolve);

        Map<String, Integer> integerMap = new HashMap<>();
        List<String> randomTags = new ArrayList<>();
        for (int j = 0; j < 3; j++) {
            Map<String, Set<String>> tagsMap = userTagRepository.getTagsMap();
            Object[] availableTags = this.strings.toArray();
            int nextInt = random.nextInt(availableTags.length);
            String randomTag = ((String)availableTags[nextInt]).toLowerCase();
            randomTags.add(randomTag);
            Set<String> strings1 = tagsMap.get(randomTag);
            for (String s : strings1) {
                Integer integer = integerMap.get(s);
                if (integer == null) {
                    integer = 1;
                } else {
                    integer++;
                }
                integerMap.put(s, integer);
            }
        }

        Integer integer = integerMap.entrySet().stream().map(Map.Entry::getValue).max(Integer::compare).get();
        int max = 0;
        String candidate = "";
        for(Map.Entry<String, Integer> entry : integerMap.entrySet()) {
            Integer value = entry.getValue();
            if (value > max) {
                candidate = entry.getKey();
                max = value;
            }
        }
        Assert.assertEquals(integer, Integer.valueOf(max));

        UserSession resolve1 = ResolvingTagsFromUserUtils.resolve(userSessions, randomTags);
        Assert.assertEquals(userRepository.getBySessionId(candidate), resolve1);
    }
}
