package com.komin.core.controllers;

import static com.komin.core.JsonConstants.SESSION_ID;

import com.google.gson.Gson;
import com.komin.core.model.UserSession;
import com.komin.core.repository.UserRepository;
import com.komin.core.services.ConnectUserWithGroupService;
import com.komin.core.utils.CheckAndConvertJsonToStringStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import java.util.Map;

@Controller
public class GroupCallController {
    private final UserRepository userRepository;
    private ConnectUserWithGroupService groupService;

    @Autowired
    public GroupCallController(UserRepository userRepository,
                               ConnectUserWithGroupService groupService) {
        this.userRepository = userRepository;
        this.groupService = groupService;
    }

    @MessageMapping("/iceCandidate")
    public void receiveIceCandidate(@Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        UserSession userSession = getUserSession(headerAccessor);
        groupService.addIceCandidateForUser(userSession, message);
    }

    @MessageMapping("/createRoom")
    public void createRoom(@Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        UserSession userSession = getUserSession(headerAccessor);
        groupService.createRoomAndJoin(userSession, message);
    }

    @MessageMapping("/joinRoom")
    public void joinRoom(@Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        UserSession userSession = getUserSession(headerAccessor);
        groupService.addNewJoinerInRoom(userSession, message);
    }

    @MessageMapping("/leaveRoom")
    public void leaveRoom(@Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        UserSession userSession = getUserSession(headerAccessor);
        groupService.leaveRoom(userSession, message);
    }

    @MessageMapping("/translateVideoFrom")
    public void startTranslate(@Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        UserSession userSession = getUserSession(headerAccessor);
        groupService.startTranslate(userSession, message);
    }

    @MessageMapping("/sendMeOtherParticipants")
    public void sendMeOtherParticipants(@Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        UserSession userSession = getUserSession(headerAccessor);
        groupService.sendMeOtherParticipants(userSession, message);
    }

    @MessageMapping("/receiveVideoFrom")
    public void receiveVideoFrom(@Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        Map objectMap = new Gson().fromJson(message, Map.class);
        String candidateSessionId = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, SESSION_ID);
        UserSession senderSession = userRepository.getBySessionId(candidateSessionId);
        UserSession userSession = getUserSession(headerAccessor);
        groupService.receiveVideoFrom(userSession, senderSession, message);
    }

    private UserSession getUserSession(SimpMessageHeaderAccessor headerAccessor) {
        String sessionId = headerAccessor.getSessionId();
        if (sessionId == null) {
            throw new IllegalArgumentException("Can't find sessionId attribute!");
        }
        if (!userRepository.exists(sessionId)) {
            throw new RuntimeException("User session is not exist! Please try to reload page");
        }
        return userRepository.getBySessionId(sessionId);
    }
}
