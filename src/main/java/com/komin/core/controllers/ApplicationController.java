package com.komin.core.controllers;

import com.komin.core.model.KominMediaPipeline;
import com.komin.core.model.KominPipelineEnum;
import com.komin.core.model.UserSession;
import com.komin.core.repository.ActiveUserRepository;
import com.komin.core.repository.UserTagRepository;
import com.komin.core.repository.UserRepository;
import com.komin.core.services.Connect2UsersService;
import com.komin.core.services.ConnectUserWithGroupService;
import com.komin.core.utils.TagRepositoryUpdaterThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Controller
public class ApplicationController {
    private final static Logger logger = LoggerFactory.getLogger(ApplicationController.class);
    private final UserTagRepository userTagRepository;
    private final UserRepository userRepository;
    private final Connect2UsersService connect2UsersService;
    private final ConnectUserWithGroupService connectUserWithGroupService;
    private final ActiveUserRepository activeUserRepository;

    @Autowired
    public ApplicationController(UserTagRepository userTagRepository,
                                 UserRepository userRepository, Connect2UsersService connect2UsersService,
                                 ConnectUserWithGroupService connectUserWithGroupService, ActiveUserRepository activeUserRepository) {
        this.userTagRepository = userTagRepository;
        this.userRepository = userRepository;
        this.connect2UsersService = connect2UsersService;
        this.connectUserWithGroupService = connectUserWithGroupService;
        this.activeUserRepository = activeUserRepository;
    }

    /*@GetMapping("/greeting")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }*/

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping("/groups")
    public String groups() {
        return "index";
    }

    @EventListener({ContextRefreshedEvent.class})
    public void contextRefreshedEvent() {
        System.out.println("a context refreshed event happened");
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.scheduleWithFixedDelay(new TagRepositoryUpdaterThread(userTagRepository, userRepository),
                10000, 5000, TimeUnit.MILLISECONDS);
    }

    @EventListener(SessionConnectEvent.class)
    public void handleWebsocketConnectListener(SessionConnectEvent event) {
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
        String sessionId = sha.getSessionId();
        logger.info("Received a new web socket connection : " + sessionId);
        if (userRepository.exists(sessionId)) {
            logger.error("Weird behaviour! Check this case!");
            userRepository.removeBySessionId(sessionId);
        }
        userRepository.register(new UserSession(sessionId));
    }

    @EventListener(SessionDisconnectEvent.class)
    public void handleWebsocketDisconnectListener(SessionDisconnectEvent event) {
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
        String sessionId = sha.getSessionId();
        UserSession userSession = userRepository.getBySessionId(sessionId);
        KominMediaPipeline pipeline = activeUserRepository.getPipeline(userSession);
        if (pipeline != null) {
            KominPipelineEnum pipelineType = pipeline.getPipelineType();
            switch (pipelineType) {
                case GROUP:
                    connectUserWithGroupService.leaveRoom(userSession);
                    break;
                case ONE_2_ONE:
                    connect2UsersService.stopCommunication(userSession);
                    break;
            }
        }
        userRepository.removeBySessionId(sessionId);
        activeUserRepository.removePipeline(userSession);
        logger.info("session closed : " + sessionId);
    }
}
