package com.komin.core.controllers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.komin.core.model.Room;
import com.komin.core.repository.RoomRepository;
import com.komin.core.services.UserTagsInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static com.komin.core.JsonConstants.*;

@org.springframework.web.bind.annotation.RestController
public class RestController {
    private final UserTagsInfoService userTagsInfoService;
    private final RoomRepository roomRepository;

    @Autowired
    public RestController(UserTagsInfoService userTagsInfoService, RoomRepository roomRepository) {
        this.userTagsInfoService = userTagsInfoService;
        this.roomRepository = roomRepository;
    }

    @RequestMapping("/appInfo")
    public String getAppInfo() {
        JsonObject infoAboutUsers = userTagsInfoService.getTagsInfo();
        return infoAboutUsers.toString();
    }

    @RequestMapping("/rooms")
    public String getRoomsInRange(@RequestParam(value = "from", defaultValue = "0") String from,
                                  @RequestParam(value = "to", defaultValue = "10") String to) {
        List<Room> sortedRooms = roomRepository.getSortedRooms();
        Integer arrayFrom = Integer.getInteger(from);
        Integer arrayTo = Integer.getInteger(to);
        List<Room> rooms = sortedRooms.subList(arrayFrom, arrayTo);
        final JsonArray roomArray = new JsonArray();
        JsonObject resultJson = new JsonObject();
        for (Room room : rooms) {
            JsonObject roomJson = new JsonObject();
            roomJson.addProperty(ROOM_NAME, room.getName());
            roomJson.addProperty(ROOM_ID, String.valueOf(room.getRoomId()));
            roomJson.addProperty(ROOM_PARTICIPANTS_NUMBER, room.getNumberOfParticipants());
            roomArray.add(roomJson);
        }
        resultJson.add(ROOMS, roomArray);
        return resultJson.toString();
    }

}
