package com.komin.core.controllers;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.komin.core.model.UserSession;
import com.komin.core.repository.UserRepository;
import com.komin.core.services.ConnectUserWithGroupService;
import com.komin.core.services.SendChatMessageService;
import com.komin.core.services.SendMessageToUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

import javax.validation.Valid;

@Controller
public class ChatController {
    private final UserRepository userRepository;
    private final SendMessageToUserService sendMessageToUserService;
    private final SendChatMessageService sendChatMessageService;
    private ConnectUserWithGroupService groupService;

    @Autowired
    public ChatController(UserRepository userRepository, SendMessageToUserService sendMessageToUserService,
                               SendChatMessageService sendChatMessageService, ConnectUserWithGroupService groupService) {
        this.userRepository = userRepository;
        this.sendMessageToUserService = sendMessageToUserService;
        this.sendChatMessageService = sendChatMessageService;
        this.groupService = groupService;
    }

    @MessageMapping("/sendMessage")
    public void chatMessage(@Valid @Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        JsonObject jsonObject = new Gson().fromJson(message, JsonObject.class);
        UserSession userSession = getUserSession(headerAccessor);
        sendChatMessageService.sendMessageToOtherUsersFromMyActiveSession(userSession, jsonObject);
    }

    private UserSession getUserSession(SimpMessageHeaderAccessor headerAccessor) {
        String sessionId = headerAccessor.getSessionId();
        if (sessionId == null) {
            throw new IllegalArgumentException("Can't find sessionId attribute!");
        }
        if (!userRepository.exists(sessionId)) {
            throw new RuntimeException("User session is not exist! Please try to reload page");
        }
        return userRepository.getBySessionId(sessionId);
    }
}
