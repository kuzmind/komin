package com.komin.core.controllers;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.komin.core.model.UserSession;
import com.komin.core.exceptions.KominConnectionException;
import com.komin.core.exceptions.KominRunTimeException;
import com.komin.core.repository.UserRepository;
import com.komin.core.services.Connect2UsersService;
import com.komin.core.services.SendMessageToUserService;
import com.komin.core.services.UserTagsInfoService;
import com.komin.core.utils.CheckAndConvertJsonToStringStrategy;
import org.kurento.client.IceCandidate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.komin.core.JsonConstants.*;

@Controller
public class One2OneCallController {
    private final UserRepository userRepository;
    private final SendMessageToUserService sendMessageToUserService;
    private final Connect2UsersService connect2UsersService;
    private final UserTagsInfoService userTagsInfoService;

    @Autowired
    public One2OneCallController(UserRepository userRepository, SendMessageToUserService sendMessageToUserService,
                                 Connect2UsersService connect2UsersService, UserTagsInfoService userTagsInfoService) {
        this.userRepository = userRepository;
        this.sendMessageToUserService = sendMessageToUserService;
        this.connect2UsersService = connect2UsersService;
        this.userTagsInfoService = userTagsInfoService;
    }

    @MessageMapping("/sdpOffer")
    public void receiveSdpOffer(@Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        String sessionId = headerAccessor.getSessionId();
        Object session = Objects.requireNonNull(headerAccessor.getSessionAttributes()).get(SESSION);
        if (sessionId == null || session == null) {
            throw new IllegalArgumentException("Can't find sessionId and session attributes!");
        }
        UserSession userSession = getUserSession(sessionId, (HttpSession) session);
        Map objectMap = new Gson().fromJson(message, Map.class);
        Object action = objectMap.get(ACTION);
        if (action == null || String.valueOf(action).equals("") || !String.valueOf(action).equals(SDP_OFFER)) {
            throw new IllegalArgumentException("Wrong action! Please check user action command");
        }
        initTags(message, userSession);
        setSdpOffer(userSession, objectMap);
        //we need to have sdpOffer and IceCandidates to connect users. If we have both - clint is ready to call
        if (!userSession.isReadyToCall()) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(ACTION, ON_ICE_CANDIDATES);
            sendMessageToUserService.sendMessage(ICE_CANDIDATES_COMMAND_ENDPOINT, jsonObject, userSession);
        } else {
            throw new KominConnectionException(sessionId, "How it's possible! Please connect with great developers");
        }
    }

    private UserSession getUserSession(String sessionId, HttpSession session) {
        UserSession userSession;
        if (!userRepository.exists(sessionId)) {
            userSession = new UserSession(session, sessionId);
            userRepository.register(userSession);
        } else {
            userSession = userRepository.getBySessionId(sessionId);
        }
        return userSession;
    }

    private void setSdpOffer(UserSession userSession, Map objectMap) {
        Object sdpOfferObject = objectMap.get(SDP_OFFER);
        if (sdpOfferObject == null) {
            throw new IllegalArgumentException("Can't find sdpOffer!");
        }
        String sdpOffer = String.valueOf(sdpOfferObject);
        userSession.setSdpOffer(sdpOffer);
        userSession.signalUserSession();
    }

    private void initTags(@Payload String message, UserSession userSession) {
        JsonArray tags = new Gson().fromJson(message, JsonObject.class).getAsJsonArray(TAGS);
        if (tags != null) {
            List<String> tagsCollection = new ArrayList<>();
            for (JsonElement jsonElement : tags) {
                JsonObject asJsonObject = jsonElement.getAsJsonObject();
                String tag = asJsonObject.get(TAG).getAsString();
                tagsCollection.add(tag.toLowerCase());
            }
            userSession.initTags(tagsCollection);
        }
    }

    @MessageMapping("/iceCandidates")
    public void receiveOnIceCandidates(@Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        String sessionId = headerAccessor.getSessionId();
        if (sessionId == null) {
            throw new IllegalArgumentException("Can't find sessionId attribute!");
        }
        if (!userRepository.exists(sessionId)) {
            throw new RuntimeException("User session is not exist! Please try to reload page");
        }
        UserSession userSession = userRepository.getBySessionId(sessionId);
        if (userSession.getSdpOffer() == null || userSession.getSdpOffer().equals("")) {
            throw new IllegalArgumentException("Can't find sdpOffer!");
        }
        JsonArray iceCandidates = new Gson().fromJson(message, JsonObject.class).getAsJsonArray(CANDIDATES);
        if (iceCandidates == null) {
            throw new IllegalArgumentException("Can't find iceCandidates!");
        }
        addIceCandidates(userSession, iceCandidates);
        setAnonymous(message, userSession);

        userSession.signalUserSession();
        if (!userSession.isReadyToCall()) {
            throw new RuntimeException("Can't start a call! isReadyToCall - false");
        }
        connect2UsersService.tryToConnectToRandomUser(userSession);
    }

    private void setAnonymous(@Payload String message, UserSession userSession) {
        Map objectMap = new Gson().fromJson(message, Map.class);
        Object anonymous = objectMap.get(ANONYMOUS);
        String isAnonymous = String.valueOf(anonymous);
        userSession.setAnonymous(Boolean.parseBoolean(isAnonymous));
    }

    private void addIceCandidates(UserSession userSession, JsonArray iceCandidates) {
        for (JsonElement jsObject : iceCandidates) {
            JsonObject asJsonObject = jsObject.getAsJsonObject();
            String candidateField = asJsonObject.get(CANDIDATE).getAsString();
            String sdpMidField = asJsonObject.get(SDP_MID).getAsString();
            int sdpMLineIndexField = asJsonObject.get(SDP_M_LINE_INDEX).getAsInt();
            IceCandidate candidate = new IceCandidate(candidateField, sdpMidField, sdpMLineIndexField);
            userSession.addCandidate(candidate);
        }
    }

    @MessageMapping("/stop")
    public void stopCommunication(@Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        String sessionId = headerAccessor.getSessionId();
        if (sessionId == null) {
            throw new IllegalArgumentException("Can't find sessionId attribute!");
        }
        if (userRepository.exists(sessionId)) {
            UserSession userSession = userRepository.getBySessionId(sessionId);
            userSession.clearTags();
            connect2UsersService.stopCommunication(userSession);
        }
    }

    @MessageExceptionHandler
    @SendToUser("/queue/errors")
    public String handleException(Throwable exception) {
        if (exception instanceof KominRunTimeException) {
            String sessionId = ((KominRunTimeException) exception).getSessionId();
            if (sessionId != null && userRepository.exists(sessionId)) {
                userRepository.removeBySessionId(sessionId);
            }
        }
        return exception.getMessage();
    }

    @MessageMapping("/deleteUser")
    public void deleteUserFromRegistry(@Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        String sessionId = headerAccessor.getSessionId();
        if (sessionId == null) {
            throw new IllegalArgumentException("Can't find sessionId attribute!");
        }
        if (userRepository.exists(sessionId)) {
            UserSession userSession = userRepository.getBySessionId(sessionId);
            connect2UsersService.stopCommunication(userSession);
            userRepository.removeBySessionId(sessionId);
        }
    }

    @MessageMapping("/getAppInfo")
    public void getInfoAboutChat(@Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        String sessionId = headerAccessor.getSessionId();
        if (sessionId == null) {
            throw new IllegalArgumentException("Can't find sessionId attribute!");
        }
        JsonObject infoAboutUsers = userTagsInfoService.getTagsInfo();
        sendMessageToUserService.sendMessage(APP_INFO_COMMAND_ENDPOINT, infoAboutUsers, sessionId);
    }

    @MessageMapping("/getTagInfo")
    public void getInfoAboutParticularTag(@Payload String message, SimpMessageHeaderAccessor headerAccessor) {
        String sessionId = headerAccessor.getSessionId();
        if (sessionId == null) {
            throw new IllegalArgumentException("Can't find sessionId attribute!");
        }
        Map objectMap = new Gson().fromJson(message, Map.class);
        String tag = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, TAG);
        JsonObject infoAboutUsers = userTagsInfoService.getTagInfo(tag.toLowerCase());
        sendMessageToUserService.sendMessage(TAG_INFO_COMMAND_ENDPOINT, infoAboutUsers, sessionId);
    }
}