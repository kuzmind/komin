package com.komin.core.entities;


public class TagImpl implements Comparable<TagImpl> {

    private final String tag;

    public String getTag() {
        return tag;
    }

    public int getCount() {
        return count;
    }

    private final int count;

    public TagImpl(String tag, int count) {
        this.tag = tag;
        this.count = count;
    }

    @Override
    public int compareTo(TagImpl o) {
        return -1 * Integer.compare(o.count, this.count);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TagImpl tag1 = (TagImpl) o;

        return count == tag1.count && tag.equals(tag1.tag);
    }

    @Override
    public int hashCode() {
        int result = tag.hashCode();
        result = 31 * result + count;
        return result;
    }
}
