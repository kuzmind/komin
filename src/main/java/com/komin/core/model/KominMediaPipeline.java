package com.komin.core.model;

import java.util.Collection;

public interface KominMediaPipeline {

    void release();

    KominPipelineEnum getPipelineType();

    Collection<String> getAllParticipantsSessionId();
}
