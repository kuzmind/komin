package com.komin.core.model;

import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.komin.core.IceCandidateFoundListener;
import com.komin.core.exceptions.RoomConnectionException;
import com.komin.core.services.SendMessageToUserService;
import org.kurento.client.Continuation;
import org.kurento.client.MediaPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import java.io.Closeable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.komin.core.ApplicationConstants.ROOM_MAX_PARTICIPANTS;
import static com.komin.core.JsonConstants.*;

public class Room implements Closeable {
    private final Logger logger = LoggerFactory.getLogger(Room.class);
    private final long roomId;
    private final int maxParticipants;
    private final String name;
    private final MediaPipeline pipeline;
    private final SendMessageToUserService messageToUserService;
    private final ConcurrentMap<String, GroupMediaPipeline> streamingParticipants = new ConcurrentHashMap<>();
    private final ArrayDeque<GroupMediaPipeline> streamingQueue = new ArrayDeque<>(100);

    private Room(long roomId, String roomName, int maxParticipants, MediaPipeline pipeline, SendMessageToUserService messageToUserService) {
        this.roomId = roomId;
        this.name = roomName;
        this.maxParticipants = maxParticipants;
        this.pipeline = Objects.requireNonNull(pipeline);
        this.messageToUserService = Objects.requireNonNull(messageToUserService);
        logger.info("ROOM {} has been created", roomName);
    }

    static Builder builder() {
        return new Builder();
    }

    public synchronized GroupMediaPipeline join(UserSession userSession, String userName) {
        logger.info("ROOM {}: adding new participant {}", this.name, userName);
        IceCandidateFoundListener outgoingEventListener = new IceCandidateFoundListener(userSession, messageToUserService);
        final GroupMediaPipeline participantPipeline = new GroupMediaPipeline(userSession, userName,
                this.pipeline, this, outgoingEventListener);
        if (streamingParticipants.size() <= maxParticipants) {
            participantPipeline.setStreamer(true);
            streamingParticipants.put(userSession.getSessionId(), participantPipeline);
        } else {
            streamingQueue.addLast(participantPipeline);
        }
        sendInfoAboutRoom(participantPipeline);
        return participantPipeline;
    }

    public void sendOtherParticipantsAndNotifyParticipants(GroupMediaPipeline userPipeline) {
        if (streamingParticipants.size() < 2) {
            return;
        }
        sendDataAboutExistingParticipants(userPipeline);
        notifyParticipantsAboutNewJoiner(userPipeline);
    }

    private void sendInfoAboutRoom(GroupMediaPipeline participantPipeline) {
        final JsonArray participantsArray = new JsonArray();
        JsonObject userJSONInfo = convertParticipantToJSON(participantPipeline);
        userJSONInfo.addProperty(IS_ME, true);
        participantsArray.add(userJSONInfo);

        final JsonObject existingParticipantsMsg = new JsonObject();
        existingParticipantsMsg.addProperty(ROOM_NAME, this.name);
        existingParticipantsMsg.addProperty(ROOM_ID, String.valueOf(this.roomId));
        existingParticipantsMsg.add(CURRENT_HOLDER, userJSONInfo);
        existingParticipantsMsg.add(PARTICIPANTS, participantsArray);
        String sessionId = participantPipeline.getUserSession().getSessionId();
        logger.debug("PARTICIPANT {}: sending a list of {} streamingParticipants", sessionId, participantsArray.size());
        messageToUserService.sendMessage(GROUP_CALL_CREATE_ROOM_ENDPOINT, existingParticipantsMsg, sessionId);
    }

    public void leave(UserSession userSession) {
        if (!streamingParticipants.containsKey(userSession.getSessionId())) {
            String format = "Can't leave room - %s. User with Id %s - Is not in the room";
            throw new RoomConnectionException(userSession.getSessionId(), String.format(format, name, userSession.getSessionId()));
        }
        logger.trace("PARTICIPANT {}: Leaving room {}", userSession.getSessionId(), this.name);
        GroupMediaPipeline mediaPipeline = streamingParticipants.get(userSession.getSessionId());
        this.removeParticipant(mediaPipeline);
        mediaPipeline.close();
    }

    private void notifyParticipantsAboutNewJoiner(GroupMediaPipeline participantPipeline) {
        final JsonObject newParticipantMsg = new JsonObject();
        newParticipantMsg.addProperty(NAME, participantPipeline.getUserName());
        newParticipantMsg.addProperty(SESSION_ID, participantPipeline.getUserSession().getSessionId());
        newParticipantMsg.addProperty(IS_STREAMER, participantPipeline.isStreamer());

        logger.trace("ROOM {}: notifying other streamingParticipants of new participant {}", name,
                participantPipeline.getUserName());

        for (final GroupMediaPipeline streamingPipeline : streamingParticipants.values()) {
            if (streamingPipeline == participantPipeline) {
                continue;
            }
            messageToUserService.sendMessage(GROUP_CALL_NEW_JOINER_ENDPOINT, newParticipantMsg, streamingPipeline.getUserSession());
        }
    }

    private JsonObject convertParticipantToJSON(GroupMediaPipeline userPipeline) {
        JsonObject participantInfo = new JsonObject();
        participantInfo.addProperty(SESSION_ID, userPipeline.getUserSession().getSessionId());
        participantInfo.addProperty(NAME, userPipeline.getUserName());
        participantInfo.addProperty(IS_STREAMER, userPipeline.isStreamer());
        return participantInfo;
    }

    private void sendDataAboutExistingParticipants(GroupMediaPipeline userPipeline) {
        final JsonArray participantsArray = new JsonArray();
        for (final GroupMediaPipeline participant : this.streamingParticipants.values()) {
            if (participant == userPipeline) {
                continue;
            }
            participantsArray.add(convertParticipantToJSON(participant));
        }
        for (final GroupMediaPipeline participant : streamingQueue) {
            if (participant == userPipeline) {
                continue;
            }
            participantsArray.add(convertParticipantToJSON(participant));
        }
        final JsonObject existingParticipantsMsg = new JsonObject();
        existingParticipantsMsg.add(PARTICIPANTS, participantsArray);
        String sessionId = userPipeline.getUserSession().getSessionId();
        logger.debug("PARTICIPANT {}: sending a list of {} streamingParticipants", sessionId, participantsArray.size());
        messageToUserService.sendMessage(GROUP_CALL_EXISTING_PARTICIPANTS_ENDPOINT, existingParticipantsMsg, sessionId);
    }

    private void removeParticipant(GroupMediaPipeline mediaPipeline) {
        String sessionId = mediaPipeline.getUserSession().getSessionId();
        streamingParticipants.remove(sessionId);
        logger.debug("ROOM {}: notifying all users that {} is leaving the room", this.name, mediaPipeline);

        final List<String> unnotifiedParticipants = new ArrayList<>();
        final JsonObject participantLeftJson = new JsonObject();
        participantLeftJson.addProperty(SESSION_ID, mediaPipeline.getUserSession().getSessionId());
        participantLeftJson.addProperty(NAME, mediaPipeline.getUserName());
        for (final GroupMediaPipeline participant : streamingParticipants.values()) {
            try {
                participant.cancelVideoFrom(mediaPipeline.getUserSession());
                messageToUserService.sendMessage(GROUP_CALL_USER_LEFT_ENDPOINT, participantLeftJson, participant.getUserSession());
            } catch (final Exception e) {
                unnotifiedParticipants.add(participant.getUserSession().getSessionId());
            }
        }

        if (!unnotifiedParticipants.isEmpty()) {
            if (logger.isDebugEnabled()) {
                logger.debug("ROOM {}: The users {} could not be notified that {} left the room", this.name,
                        unnotifiedParticipants, mediaPipeline);
            }
        }
    }

    public Collection<GroupMediaPipeline> getParticipantsPipelines() {
        return streamingParticipants.values();
    }

    public GroupMediaPipeline getParticipant(String sessionId) {
        return streamingParticipants.get(name);
    }

    public GroupMediaPipeline getParticipant(UserSession userSession) {
        return getParticipant(userSession.getSessionId());
    }

    public String getName() {
        return name;
    }

    @PreDestroy
    private void shutdown() {
        this.close();
    }

    @Override
    public void close() {
        for (final GroupMediaPipeline groupMediaPipeline : streamingParticipants.values()) {
            groupMediaPipeline.release();
        }

        streamingParticipants.clear();

        pipeline.release(new Continuation<Void>() {

            @Override
            public void onSuccess(Void result) {
                logger.trace("ROOM {}: Released Pipeline", Room.this.name);
            }

            @Override
            public void onError(Throwable cause) {
                logger.warn("PARTICIPANT {}: Could not release Pipeline", Room.this.name);
            }
        });

        logger.debug("Room {} closed", this.name);
    }

    public long getRoomId() {
        return roomId;
    }

    public int getNumberOfParticipants() {
        return streamingParticipants.size() + streamingQueue.size();
    }

    static class Builder {
        private long roomId;
        private int maxParticipants = ROOM_MAX_PARTICIPANTS;
        private String name;
        private MediaPipeline pipeline;
        private SendMessageToUserService messageToUserService;

        Builder roomId(long roomId) {
            this.roomId = roomId;
            return this;
        }

        Builder maxParticipants(int maxParticipants) {
            this.maxParticipants = maxParticipants;
            return this;
        }

        Builder name(String name) {
            this.name = name;
            return this;
        }

        Builder pipeline(MediaPipeline pipeline) {
            this.pipeline = pipeline;
            return this;
        }

        Builder messageToUserService(SendMessageToUserService messageToUserService) {
            this.messageToUserService = messageToUserService;
            return this;
        }

        Room build() {
            Objects.requireNonNull(this.messageToUserService);
            Objects.requireNonNull(this.pipeline);
            assert Strings.isNullOrEmpty(this.name);
            return new Room(roomId, name, maxParticipants, pipeline, messageToUserService);
        }
    }
}
