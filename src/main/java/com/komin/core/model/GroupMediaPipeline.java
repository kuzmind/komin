package com.komin.core.model;

import org.kurento.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class GroupMediaPipeline implements KominMediaPipeline, Closeable {
    private final Logger logger = LoggerFactory.getLogger(GroupMediaPipeline.class);
    private final UserSession userSession;
    private final WebRtcEndpoint outgoingMedia;
    private final ConcurrentMap<String, WebRtcEndpoint> incomingMedia = new ConcurrentHashMap<>();
    private final String name;
    private final MediaPipeline pipeline;
    private final Room room;
    private boolean isStreamer = false;

    public GroupMediaPipeline(UserSession userSession, String name, MediaPipeline mediaPipeline, Room room,
                              EventListener<IceCandidateFoundEvent> outgoingEventListener) {
        this.userSession = userSession;
        this.name = name;
        this.pipeline = mediaPipeline;
        this.room = room;
        this.outgoingMedia = new WebRtcEndpoint.Builder(pipeline).build();
        this.outgoingMedia.addIceCandidateFoundListener(outgoingEventListener);
    }

    public WebRtcEndpoint getOutgoingWebRtcPeer() {
        return outgoingMedia;
    }

    public UserSession getUserSession() {return userSession;}

    public WebRtcEndpoint getEndpointForUser(final UserSession sender,
                                              EventListener<IceCandidateFoundEvent> foundEventEventListener) {
        String userSessionId = this.getUserSession().getSessionId();
        if (sender == userSession) {
            logger.trace("PARTICIPANT {}: configuring loopback", userSessionId);
            return outgoingMedia;
        }

        String senderSessionId = sender.getSessionId();
        logger.trace("PARTICIPANT {}: receiving video from {}", userSessionId, senderSessionId);
        WebRtcEndpoint incoming = incomingMedia.get(senderSessionId);
        if (incoming == null) {
            logger.trace("PARTICIPANT {}: creating new endpoint from {}", userSessionId, senderSessionId);
            incoming = new WebRtcEndpoint.Builder(pipeline).build();
            incoming.addIceCandidateFoundListener(foundEventEventListener);
            incomingMedia.put(senderSessionId, incoming);
        }
        return incoming;
    }

    public void cancelVideoFrom(final UserSession sender) {
        String userSessionId = this.getUserSession().getSessionId();
        String senderSessionId = sender.getSessionId();
        logger.trace("PARTICIPANT {}: canceling video from {}", userSessionId, senderSessionId);
        WebRtcEndpoint incoming = incomingMedia.get(senderSessionId);
        incoming.release(new Continuation<Void>() {
            @Override
            public void onSuccess(Void result) throws Exception {
                logger.trace("PARTICIPANT {}: Released successfully incoming EP for {}",
                        userSessionId, senderSessionId);
            }

            @Override
            public void onError(Throwable cause) throws Exception {
                logger.warn("PARTICIPANT {}: Could not release incoming EP for {}", userSessionId,
                        senderSessionId);
            }
        });
    }

    public void release() {
        String userSessionId = this.getUserSession().getSessionId();
        logger.debug("PARTICIPANT {}: Releasing resources", userSessionId);
        for (final String remoteParticipantSessionId : incomingMedia.keySet()) {

            logger.trace("PARTICIPANT {}: Released incoming EP for {}", userSessionId, remoteParticipantSessionId);

            final WebRtcEndpoint ep = this.incomingMedia.get(remoteParticipantSessionId);

            ep.release(new Continuation<Void>() {

                @Override
                public void onSuccess(Void result) throws Exception {
                    logger.trace("PARTICIPANT {}: Released successfully incoming EP for {}",
                            userSessionId, remoteParticipantSessionId);
                }

                @Override
                public void onError(Throwable cause) throws Exception {
                    logger.warn("PARTICIPANT {}: Could not release incoming EP for {}", userSessionId,
                            remoteParticipantSessionId);
                }
            });
        }

        outgoingMedia.release(new Continuation<Void>() {

            @Override
            public void onSuccess(Void result) throws Exception {
                logger.trace("PARTICIPANT {}: Released outgoing EP", userSessionId);
            }

            @Override
            public void onError(Throwable cause) throws Exception {
                logger.warn("USER {}: Could not release outgoing EP", userSessionId);
            }
        });
    }

    @Override
    public KominPipelineEnum getPipelineType() {
        return KominPipelineEnum.GROUP;
    }

    @Override
    public Collection<String> getAllParticipantsSessionId() {
        ArrayList<String> sessions = new ArrayList<>(incomingMedia.keySet());
        sessions.add(userSession.getSessionId());
        return sessions;
    }

    public Map<String, WebRtcEndpoint> getIncomingMedia() {
        return incomingMedia;
    }

    public Room getRoom() {
        return this.room;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof GroupMediaPipeline)) {
            return false;
        }
        GroupMediaPipeline other = (GroupMediaPipeline) obj;
        boolean eq = this.userSession.getSessionId().equals(other.getUserSession().getSessionId());
        eq &= this.room.getName().equals(other.getRoom().getName());
        return eq;
    }

    public String getUserName() {
        return name;
    }

    public boolean isStreamer() {
        return isStreamer;
    }

    public void setStreamer(boolean isStreamer) {
        this.isStreamer = isStreamer;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + this.userSession.getSessionId().hashCode();
        result = 31 * result + this.room.getName().hashCode();
        return result;
    }

    @Override
    public String toString() {
        String FORMAT_TO_STRING = "Pipeline id - %s for Room - %s";
        return String.format(FORMAT_TO_STRING, this.userSession.getSessionId(), this.room.getName());
    }

    @Override
    public void close() {
        release();
    }

    public void addIceCandidate(IceCandidate iceCandidate, String candidateSessionId) {
        if (this.userSession.getSessionId().compareTo(candidateSessionId) == 0) {
            outgoingMedia.addIceCandidate(iceCandidate);
        } else {
            WebRtcEndpoint webRtc = incomingMedia.get(candidateSessionId);
            if (webRtc != null) {
                webRtc.addIceCandidate(iceCandidate);
            }
        }
    }
}
