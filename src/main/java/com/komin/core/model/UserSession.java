/*
 * (C) Copyright 2014 Kurento (http://kurento.org/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.komin.core.model;

import org.kurento.client.IceCandidate;
import org.kurento.client.WebRtcEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * User session.
 *
 */
public class UserSession {

  private static final Logger log = LoggerFactory.getLogger(UserSession.class);

  private final String sessionId;
  private HttpSession session;

  private String sdpOffer;
  private boolean isReadyToCall;
  private WebRtcEndpoint webRtcEndpoint;
  private final List<IceCandidate> candidateList = new ArrayList<IceCandidate>();
  private final Set<String> tags = new HashSet<>();

  private boolean anonymous;

  public UserSession(HttpSession session, String sessionId) {
    this.session = session;
    this.sessionId = sessionId;
  }

  public UserSession(String sessionId) {
    this.sessionId = sessionId;
  }

  public Optional<HttpSession> getSession() {
    return Optional.of(session);
  }

  /**
   * Not implemented yet
   * @return address of client
   */
  @Deprecated
  public String getAddress() {
    return session.getServletContext().getServerInfo();
  }

  public String getSdpOffer() {
    return sdpOffer;
  }

  public void setSdpOffer(String sdpOffer) {
    this.sdpOffer = sdpOffer;
  }

  public String getSessionId() {
    return sessionId;
  }

  public void setWebRtcEndpoint(WebRtcEndpoint webRtcEndpoint) {
    this.webRtcEndpoint = webRtcEndpoint;

    for (IceCandidate e : candidateList) {
      this.webRtcEndpoint.addIceCandidate(e);
    }
    this.candidateList.clear();
  }

  public void addCandidate(IceCandidate candidate) {
    if (this.webRtcEndpoint != null) {
      this.webRtcEndpoint.addIceCandidate(candidate);
    } else {
      candidateList.add(candidate);
    }
  }

  public void clear() {
    this.webRtcEndpoint = null;
    this.candidateList.clear();
    this.setReadyToCall(false);
    this.tags.clear();
  }

  public void initTags(Collection<String> tags) {
    if (!this.tags.isEmpty()) {
      this.tags.clear();
    }
    this.tags.addAll(tags);
  }

  public Collection<String> getTags() {
    return this.tags;
  }

  public void clearTags() {
    this.tags.clear();
  }

  /**
   * Method that should be called after we receive sdpOffer or OnIceCandidates. Signal that client is ready for call
   */
  public void signalUserSession() {
    if (!candidateList.isEmpty() && sdpOffer != null && !sdpOffer.equals("")) {
      setReadyToCall(true);
    }
  }

  public boolean isReadyToCall() {
    return isReadyToCall;
  }

  private void setReadyToCall(boolean readyToCall) {
    isReadyToCall = readyToCall;
  }

  public boolean isAnonymous() {
    return anonymous;
  }

  public void setAnonymous(boolean anonymous) {
    this.anonymous = anonymous;
  }

  @Override
  public String toString() {
    return "UserSession{" +
            "sessionId='" + sessionId + '\'' +
            ", isReadyToCall=" + isReadyToCall +
            ", tags=" + tags +
            ", anonymous=" + anonymous +
            '}';
  }
}
