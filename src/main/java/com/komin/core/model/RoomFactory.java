package com.komin.core.model;

import com.komin.core.services.SendMessageToUserService;
import org.kurento.client.KurentoClient;

import java.util.concurrent.atomic.AtomicLong;

public class RoomFactory {
    private final KurentoClient kurentoClient;
    private final SendMessageToUserService messageToUserService;
    private final int maxParticipants;
    private final AtomicLong idGenerator = new AtomicLong(0L);

    public RoomFactory(KurentoClient kurentoClient, SendMessageToUserService messageToUserService, int maxParticipants) {
        this.kurentoClient = kurentoClient;
        this.messageToUserService = messageToUserService;
        this.maxParticipants = maxParticipants;
    }

    public synchronized Room createRoom(String roomName) {
        long roomId = idGenerator.getAndIncrement();
        return Room.builder()
                .roomId(roomId)
                .maxParticipants(maxParticipants)
                .name(roomName)
                .messageToUserService(messageToUserService)
                .pipeline(kurentoClient.createMediaPipeline())
                .build();
    }
}
