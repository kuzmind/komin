/*
 * (C) Copyright 2014 Kurento (http://kurento.org/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.komin.core.model;

import org.kurento.client.KurentoClient;
import org.kurento.client.MediaPipeline;
import org.kurento.client.WebRtcEndpoint;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Media Pipeline (WebRTC endpoints, i.e. Kurento Media Elements) and connections for the 1 to 1
 * video communication.
 *
 */
public class One2OneMediaPipeline implements KominMediaPipeline {

  private final UserSession firstSession;
  private final UserSession secondSession;
  private MediaPipeline pipeline;
  private WebRtcEndpoint firstWebRtcEp;
  private WebRtcEndpoint secondWebRtcEp;

  public One2OneMediaPipeline(KurentoClient kurento, UserSession firstSession, UserSession secondSession) {
    this.firstSession = firstSession;
    this.secondSession = secondSession;
    try {
      this.pipeline = kurento.createMediaPipeline();
      this.firstWebRtcEp = new WebRtcEndpoint.Builder(pipeline).build();
      this.secondWebRtcEp = new WebRtcEndpoint.Builder(pipeline).build();

      this.firstWebRtcEp.connect(this.secondWebRtcEp);
      this.secondWebRtcEp.connect(this.firstWebRtcEp);
    } catch (Throwable t) {
      if (this.pipeline != null) {
        pipeline.release();
      }
    }
  }

  public String generateSdpAnswerForFirst(String sdpOffer) {
    return firstWebRtcEp.processOffer(sdpOffer);
  }

  public String generateSdpAnswerForSecond(String sdpOffer) {
    return secondWebRtcEp.processOffer(sdpOffer);
  }

  public void release() {
    if (pipeline != null) {
      pipeline.release();
    }
  }

  @Override
  public KominPipelineEnum getPipelineType() {
    return KominPipelineEnum.ONE_2_ONE;
  }

  @Override
  public Collection<String> getAllParticipantsSessionId() {
    ArrayList<String> sessions = new ArrayList<>();
    sessions.add(firstSession.getSessionId());
    sessions.add(secondSession.getSessionId());
    return sessions;
  }

  public UserSession getFirstSession() {
    return firstSession;
  }

  public UserSession getSecondSession() {
    return secondSession;
  }

  public WebRtcEndpoint getFirstWebRtcEp() {
    return firstWebRtcEp;
  }

  public WebRtcEndpoint getSecondWebRtcEp() {
    return secondWebRtcEp;
  }

  public MediaPipeline getPipeline() {
    return pipeline;
  }
}
