package com.komin.core.services;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.komin.core.*;
import com.komin.core.exceptions.PipelineNotFoundException;
import com.komin.core.exceptions.RoomConnectionException;
import com.komin.core.exceptions.RoomNotFindException;
import com.komin.core.model.GroupMediaPipeline;
import com.komin.core.model.KominMediaPipeline;
import com.komin.core.model.KominPipelineEnum;
import com.komin.core.model.Room;
import com.komin.core.model.RoomFactory;
import com.komin.core.model.UserSession;
import com.komin.core.repository.ActiveUserRepository;
import com.komin.core.repository.RoomRepository;
import com.komin.core.utils.CheckAndConvertJsonToStringStrategy;
import org.kurento.client.IceCandidate;
import org.kurento.client.WebRtcEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import static com.komin.core.JsonConstants.*;

@Service
public class ConnectUserWithGroupService {
    private final Logger logger = LoggerFactory.getLogger(ConnectUserWithGroupService.class);
    private final ActiveUserRepository activeUserRepository;
    private final SendMessageToUserService messageService;
    private final RoomRepository roomRepository;
    private final RoomFactory roomFactory;

    @Autowired
    public ConnectUserWithGroupService(ActiveUserRepository activeUserRepository, SendMessageToUserService messageService,
                                       RoomRepository roomRepository, RoomFactory roomFactory) {
        this.activeUserRepository = activeUserRepository;
        this.messageService = messageService;
        this.roomRepository = roomRepository;
        this.roomFactory = roomFactory;
    }

    public void addIceCandidateForUser(UserSession userSession, String message) {
        String sessionId = userSession.getSessionId();
        if (logger.isTraceEnabled()) {
            logger.trace("Trying to add IceCandidate to userSession {}", sessionId);
        }
        GroupMediaPipeline pipeline = getGroupMediaPipelineForUser(userSession);
        Map objectMap = new Gson().fromJson(message, Map.class);
        String candidateSessionId = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, SESSION_ID);
        final JsonObject jsonMessage = new Gson().fromJson(message, JsonObject.class);
        JsonObject candidate = jsonMessage.get(ICE_CANDIDATE).getAsJsonObject();
        IceCandidate iceCandidate = new IceCandidate(candidate.get(CANDIDATE).getAsString(),
                candidate.get(SDP_MID).getAsString(), candidate.get(SDP_M_LINE_INDEX).getAsInt());
        pipeline.addIceCandidate(iceCandidate, candidateSessionId);
        if (logger.isInfoEnabled()) {
            logger.info("Successfully added IceCandidate to sessionId {}", sessionId);
        }

    }

    private void addNewJoinerInRoom(UserSession userSession, String roomName, String userName, Long roomId) {
        if (logger.isTraceEnabled()) {
            logger.trace("Trying to add new Joiner {} into Room {}", userName, roomName);
        }
        Room room = roomRepository.getRoom(roomId);
        if (room == null) {
            throw new RoomNotFindException(userSession.getSessionId(), roomName, roomId);
        }
        GroupMediaPipeline groupMediaPipeline = room.join(userSession, userName);
        activeUserRepository.registerPipeline(userSession, groupMediaPipeline);
        if (logger.isInfoEnabled()) {
            logger.info("Successfully added user {} to Room {}", userName, roomName);
        }
    }

    public void addNewJoinerInRoom(UserSession userSession, String message) {
        Map objectMap = new Gson().fromJson(message, Map.class);
        String roomName = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, ROOM_NAME);
        String roomId = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, ROOM_ID);
        String userName = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, PARTICIPANT_NAME);
        addNewJoinerInRoom(userSession, roomName, userName, Long.parseLong(roomId));
    }

    private void leaveRoom(UserSession userSession, Room room) {
        String roomName = room.getName();
        if (logger.isTraceEnabled()) {
            logger.trace("User {} is trying to leave Room {} ", userSession.getSessionId(), roomName);
        }
        room.leave(userSession);
        if (room.getParticipantsPipelines().isEmpty()) {
            if (logger.isInfoEnabled()) {
                logger.info("Removing empty Room {}", roomName);
            }
            Room roomToRemove = roomRepository.removeRoom(room.getRoomId());
            roomToRemove.close();
        }
    }

    public void leaveRoom(UserSession userSession) {
        GroupMediaPipeline pipeline = getGroupMediaPipelineForUser(userSession);
        Room room = pipeline.getRoom();
        if (roomRepository.exist(room)) {
            leaveRoom(userSession, room);
        }
    }

    public void leaveRoom(UserSession userSession, String message) {
        Map objectMap = new Gson().fromJson(message, Map.class);
        GroupMediaPipeline groupMediaPipelineForUser = getGroupMediaPipelineForUser(userSession);
        String roomName = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, ROOM_NAME);
        String roomId = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, ROOM_ID);
        Room room = groupMediaPipelineForUser.getRoom();
        if (room == null) {
            throw new RoomConnectionException(userSession.getSessionId(), roomName, Long.parseLong(roomId));
        }
        leaveRoom(userSession, room);
    }

    public void receiveVideoFrom(UserSession userSession, UserSession senderSession, String messageFromSender) {
        String sessionId = userSession.getSessionId();
        String senderSessionId = senderSession.getSessionId();
        if (logger.isInfoEnabled()) {
            logger.info("User {} is receiving video from {} ", sessionId, senderSessionId);
        }
        if (sessionId.equals(senderSessionId)) {
            return;
        }
        Map objectMap = new Gson().fromJson(messageFromSender, Map.class);
        String senderName = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, NAME);
        String sdpOffer = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, SDP_OFFER);
        //todo include this logic into Room class
        GroupMediaPipeline groupMediaPipeline = getGroupMediaPipelineForUser(userSession);
        GroupMediaPipeline presenterPipeline = getGroupMediaPipelineForUser(senderSession);
        if (groupMediaPipeline.getRoom() != presenterPipeline.getRoom()) {
            logger.error("Users {} and {} have to be in the same room!", sessionId, senderSessionId);
            throw new RoomConnectionException(sessionId, "Users have to be in the same room!");
        }
        IceCandidateFoundListener foundEventEventListener = new IceCandidateFoundListener(userSession, messageService);
        foundEventEventListener.addStringPropertyToJSon(NAME, senderName);
        foundEventEventListener.addStringPropertyToJSon(SESSION_ID, senderSessionId);
        WebRtcEndpoint endpointForSenderInMyRoom = groupMediaPipeline.getEndpointForUser(senderSession, foundEventEventListener);
        presenterPipeline.getOutgoingWebRtcPeer().connect(endpointForSenderInMyRoom);
        String sdpAnswer = endpointForSenderInMyRoom.processOffer(sdpOffer);
        final JsonObject scParams = new JsonObject();
        scParams.addProperty(NAME, senderName);
        scParams.addProperty(SESSION_ID, senderSessionId);
        scParams.addProperty(SDP_ANSWER, sdpAnswer);
        if (logger.isInfoEnabled()) {
            logger.info("User {} has successfully received video from {} ", sessionId, senderSessionId);
        }
        messageService.sendMessage(GROUP_CALL_RECEIVE_VIDEO_FROM_ENDPOINT, scParams, userSession);
        endpointForSenderInMyRoom.gatherCandidates();
        if (logger.isInfoEnabled()) {
            logger.info("User {} has successfully received video from {}", sessionId, senderSessionId);
        }
    }

    private GroupMediaPipeline getGroupMediaPipelineForUser(UserSession senderSession) {
        KominMediaPipeline presenterPipeline = activeUserRepository.getPipeline(senderSession);
        if (presenterPipeline == null || presenterPipeline.getPipelineType() != KominPipelineEnum.GROUP) {
            String sessionId = senderSession.getSessionId();
            logger.error("Can't find pipeline to receive a video for user {}!", sessionId);
            throw new PipelineNotFoundException(sessionId);
        }
        return (GroupMediaPipeline) presenterPipeline;
    }

    public void startTranslate(UserSession userSession, String message) {
        Map objectMap = new Gson().fromJson(message, Map.class);
        String sdpOffer = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, SDP_OFFER);
        GroupMediaPipeline streamerPipeline = getGroupMediaPipelineForUser(userSession);
        String sessionId = userSession.getSessionId();
        WebRtcEndpoint streamerEndPoint = streamerPipeline.getOutgoingWebRtcPeer();
        String sdpAnswer = streamerEndPoint.processOffer(sdpOffer);
        final JsonObject scParams = new JsonObject();
        scParams.addProperty(SESSION_ID, sessionId);
        scParams.addProperty(SDP_ANSWER, sdpAnswer);
        messageService.sendMessage(GROUP_CALL_RECEIVE_VIDEO_FROM_ENDPOINT, scParams, userSession);
        streamerEndPoint.gatherCandidates();
        if (logger.isInfoEnabled()) {
            logger.info("Streamer {} has successfully started video", sessionId);
        }
    }

    public void createRoomAndJoin(UserSession userSession, String message) {
        Map objectMap = new Gson().fromJson(message, Map.class);
        String roomName = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, ROOM_NAME);
        String userName = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, PARTICIPANT_NAME);
        Room room = roomFactory.createRoom(roomName);
        roomRepository.addRoom(room);
        addNewJoinerInRoom(userSession, roomName, userName, room.getRoomId());
    }

    public void sendMeOtherParticipants(UserSession userSession, String message) {
        Map objectMap = new Gson().fromJson(message, Map.class);
        String roomId = CheckAndConvertJsonToStringStrategy.checkAndGet(objectMap, ROOM_ID);
        Room room = roomRepository.getRoom(Long.parseLong(roomId));
        if (room == null) {
            throw new RoomConnectionException(userSession.getSessionId(), "Room can't be null!");
        }
        GroupMediaPipeline userPipeline = getGroupMediaPipelineForUser(userSession);
        room.sendOtherParticipantsAndNotifyParticipants(userPipeline);
    }
}
