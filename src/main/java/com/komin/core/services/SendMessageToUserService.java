package com.komin.core.services;

import com.google.gson.JsonObject;
import com.komin.core.model.UserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class SendMessageToUserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SendMessageToUserService.class);
    private final SimpMessageSendingOperations messagingTemplate;

    @Autowired
    public SendMessageToUserService(SimpMessageSendingOperations messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    public void sendMessage(String destination, JsonObject jsonObject, String sessionId) {
        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor
                .create(SimpMessageType.MESSAGE);
        headerAccessor.setSessionId(sessionId);
        headerAccessor.setLeaveMutable(true);
        String message = jsonObject.toString();
        LOGGER.debug(String.format("Sending message to sessionId - %s; destination - %s; message - %s", sessionId, destination, message));
        messagingTemplate.convertAndSendToUser(sessionId, destination, message, headerAccessor.getMessageHeaders());
    }

    public void sendMessage(String destination, JsonObject jsonObject, UserSession userSession) {
        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor
                .create(SimpMessageType.MESSAGE);
        String sessionId = userSession.getSessionId();
        headerAccessor.setSessionId(sessionId);
        headerAccessor.setLeaveMutable(true);
        String message = jsonObject.toString();
        LOGGER.debug(String.format("Sending message to sessionId - %s; destination - %s; message - %s", sessionId, destination, message));
        messagingTemplate.convertAndSendToUser(sessionId, destination, message, headerAccessor.getMessageHeaders());
    }

    public void sendMessage(String destination, JsonObject jsonObject, Collection<UserSession> userSessions) {
        userSessions.forEach(userSession -> {
            sendMessage(destination, jsonObject, userSession);
        });
    }
}
