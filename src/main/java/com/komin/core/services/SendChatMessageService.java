package com.komin.core.services;

import static com.komin.core.JsonConstants.NEW_MESSAGE_ENDPOINT;
import static com.komin.core.JsonConstants.SESSION_ID;

import com.google.gson.JsonObject;
import com.komin.core.model.KominMediaPipeline;
import com.komin.core.model.UserSession;
import com.komin.core.repository.ActiveUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class SendChatMessageService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SendChatMessageService.class);
    private final SendMessageToUserService sendMessageToUserService;
    private final ActiveUserRepository activeUserRepository;

    @Autowired
    public SendChatMessageService(SendMessageToUserService sendMessageToUserService, ActiveUserRepository activeUserRepository) {
        this.sendMessageToUserService = sendMessageToUserService;
        this.activeUserRepository = activeUserRepository;
    }

    public void sendMessageToOtherUsersFromMyActiveSession(UserSession userSession, JsonObject jsonObject) {
        KominMediaPipeline pipeline = activeUserRepository.getPipeline(userSession);
        if (pipeline == null) {
            LOGGER.error("This user is not active user! Can't send message!");
            return;
        }
        jsonObject.addProperty(SESSION_ID, userSession.getSessionId());
        Collection<String> allParticipantsSessionId = pipeline.getAllParticipantsSessionId();
        for (String sessionId : allParticipantsSessionId) {
            if (!sessionId.equals(userSession.getSessionId())) {
                sendMessageToUserService.sendMessage(NEW_MESSAGE_ENDPOINT, jsonObject, sessionId);
            }
        }
    }
}
