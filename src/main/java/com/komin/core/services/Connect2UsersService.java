package com.komin.core.services;

import com.google.common.base.Strings;
import com.google.gson.JsonObject;
import com.komin.core.*;
import com.komin.core.model.KominMediaPipeline;
import com.komin.core.model.KominPipelineEnum;
import com.komin.core.model.One2OneMediaPipeline;
import com.komin.core.model.UserSession;
import com.komin.core.repository.ActiveUserRepository;
import org.kurento.client.FaceOverlayFilter;
import org.kurento.client.KurentoClient;
import org.kurento.client.WebRtcEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.komin.core.JsonConstants.*;

@Service
public class Connect2UsersService {
    private static final Logger LOGGER = LoggerFactory.getLogger(Connect2UsersService.class);
    private final ActiveUserRepository activeUserRepository;
    private final SendMessageToUserService messageService;
    private final KurentoClient kurentoClient;

    @Autowired
    public Connect2UsersService(ActiveUserRepository activeUserRepository, SendMessageToUserService messageService,
                                KurentoClient kurentoClient) {
        this.activeUserRepository = activeUserRepository;
        this.messageService = messageService;
        this.kurentoClient = kurentoClient;
    }

    public void tryToConnectToRandomUser(UserSession userSession) {
        UserSession candidate = activeUserRepository.findCandidate(userSession);
        if (candidate == null) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(ACTION, WAIT);
            messageService.sendMessage(WAIT_COMMAND_ENDPOINT, jsonObject, userSession);
        } else {
            tryToConnectToParticularUser(userSession, candidate);
        }
    }

    /**
     * Connect two users
     *
     * @param firstUser
     * @param secondUser
     */
    public void tryToConnectToParticularUser(UserSession firstUser, UserSession secondUser) {
        One2OneMediaPipeline pipeline = null;
        final JsonObject startCommunicationForSecond = new JsonObject();
        final JsonObject startCommunicationForFirst = new JsonObject();
        try {
            pipeline = new One2OneMediaPipeline(kurentoClient, firstUser, secondUser);
            activeUserRepository.registerPipeline(firstUser, pipeline);
            activeUserRepository.registerPipeline(secondUser, pipeline);

            WebRtcEndpoint secondWebRtcEp = pipeline.getSecondWebRtcEp();
            secondUser.setWebRtcEndpoint(secondWebRtcEp);
            IceCandidateFoundListener iceCandidateFoundListenerForSecondUser = new IceCandidateFoundListener(secondUser, messageService);
            secondWebRtcEp.addIceCandidateFoundListener(iceCandidateFoundListenerForSecondUser);

            WebRtcEndpoint firstWebRtcEp = pipeline.getFirstWebRtcEp();
            firstUser.setWebRtcEndpoint(firstWebRtcEp);
            IceCandidateFoundListener iceCandidateFoundListenerForFirstUser = new IceCandidateFoundListener(firstUser, messageService);
            firstWebRtcEp.addIceCandidateFoundListener(iceCandidateFoundListenerForFirstUser);

            if (secondUser.isAnonymous()) {
                //add filter to pipeline
                Optional<FaceOverlayFilter> filterOptional = getFaceOverlayFilter(pipeline);
                filterOptional.ifPresent(faceOverlayFilter -> {
                    secondWebRtcEp.connect(faceOverlayFilter);
                    faceOverlayFilter.connect(firstWebRtcEp);
                });
            }
            String secondSdpAnswer = pipeline.generateSdpAnswerForSecond(secondUser.getSdpOffer());
            startCommunicationForSecond.addProperty(SDP_ANSWER, secondSdpAnswer);
            secondWebRtcEp.gatherCandidates();

            if (firstUser.isAnonymous()) {
                //add filter to pipeline
                Optional<FaceOverlayFilter> filterOptional = getFaceOverlayFilter(pipeline);
                filterOptional.ifPresent(faceOverlayFilter -> {
                    firstWebRtcEp.connect(faceOverlayFilter);
                    faceOverlayFilter.connect(secondWebRtcEp);
                });
            }
            String firstSdpAnswer = pipeline.generateSdpAnswerForFirst(firstUser.getSdpOffer());
            startCommunicationForFirst.addProperty(SDP_ANSWER, firstSdpAnswer);
            firstWebRtcEp.gatherCandidates();

            messageService.sendMessage(CONNECT_COMMAND_ENDPOINT, startCommunicationForFirst, firstUser);
            messageService.sendMessage(CONNECT_COMMAND_ENDPOINT, startCommunicationForSecond, secondUser);

        } catch (Throwable t) {
            LOGGER.error(t.getMessage(), t);
            t.printStackTrace();

            if (pipeline != null) {
                pipeline.release();
            }

            activeUserRepository.removePipeline(firstUser);
            activeUserRepository.removeUserFromQueue(firstUser);
            activeUserRepository.removePipeline(secondUser);
            activeUserRepository.removeUserFromQueue(secondUser);

            //todo implement logic of re-try with other candidates (send to stopAndCall method)
            messageService.sendMessage(STOP_COMMAND_ENDPOINT, new JsonObject(), firstUser);
            messageService.sendMessage(STOP_COMMAND_ENDPOINT, new JsonObject(), secondUser);
        }
    }

    private Optional<FaceOverlayFilter> getFaceOverlayFilter(One2OneMediaPipeline pipeline) {
        //add filter to pipeline
        //todo add AI property here
        String imageUrl = System.getProperty("komin.face.image");
        if (Strings.isNullOrEmpty(imageUrl)) {
            return Optional.empty();
        }
        FaceOverlayFilter faceOverlayFilter = new FaceOverlayFilter.Builder(pipeline.getPipeline()).build();

        faceOverlayFilter.setOverlayedImage(imageUrl, 0.0F, 0.0F,
                1.5F, 1.5F);
        return Optional.of(faceOverlayFilter);
    }

    public void stopCommunication(UserSession stopperSession) {
        //todo think about this part - refactoring?
        activeUserRepository.removeUserFromQueue(stopperSession);
        stopperSession.clear();
        if (activeUserRepository.existPipeline(stopperSession)) {
            KominMediaPipeline pipeline = activeUserRepository.getPipeline(stopperSession);
            if (pipeline.getPipelineType() == KominPipelineEnum.ONE_2_ONE) {
                UserSession firstSession = ((One2OneMediaPipeline)pipeline).getFirstSession();
                activeUserRepository.removeUserFromQueue(firstSession);
                firstSession.clear();

                UserSession secondSession = ((One2OneMediaPipeline)pipeline).getSecondSession();
                activeUserRepository.removeUserFromQueue(secondSession);
                secondSession.clear();

                activeUserRepository.removePipeline(firstSession);
                activeUserRepository.removePipeline(secondSession);

                JsonObject stopMessage = new JsonObject();
                stopMessage.addProperty(ACTION, STOP);

                UserSession stoppedSession = stopperSession == firstSession ? secondSession : firstSession;

                messageService.sendMessage(STOP_COMMAND_ENDPOINT, stopMessage, stoppedSession);
            }

            pipeline.release();
        }
    }
}
