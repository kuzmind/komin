package com.komin.core.services;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.komin.core.entities.TagImpl;
import com.komin.core.repository.UserTagRepository;
import com.komin.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

import static com.komin.core.JsonConstants.*;

@Service
public class UserTagsInfoService {

    private final UserRepository userRepository;
    private final UserTagRepository userTagRepository;

    @Autowired
    public UserTagsInfoService(UserRepository userRepository, UserTagRepository userTagRepository) {
        this.userRepository = userRepository;
        this.userTagRepository = userTagRepository;
    }

    public JsonObject getTagsInfo() {
        JsonObject resultJsonObject = new JsonObject();
        int userCount = userRepository.getUserCount();
        resultJsonObject.addProperty(USERS_COUNT, userCount);
        int usersWithTags = userTagRepository.getUsersWithTags().size();
        resultJsonObject.addProperty(USERS_WITH_TAGS, usersWithTags);
        Collection<TagImpl> mostPopularTags = userTagRepository.getMostPopularTags();
        JsonArray jsonArray = new JsonArray();
        for (TagImpl tag : mostPopularTags) {
            int count = tag.getCount();
            if (count > 0) {
                JsonObject tagObject = new JsonObject();
                tagObject.addProperty(TAG, tag.getTag());
                tagObject.addProperty(COUNT, tag.getCount());
                jsonArray.add(tagObject);
            }
        }
        resultJsonObject.add(MOST_POPULAR_TAGS, jsonArray);
        return resultJsonObject;
    }

    public JsonObject getTagInfo(String tag) {
        JsonObject jsonObject = new JsonObject();
        int numberOfUsers = userTagRepository.countOfUsersWithParticularTag(tag);
        jsonObject.addProperty(TAG, tag);
        jsonObject.addProperty(COUNT, numberOfUsers);
        return jsonObject;
    }
}
