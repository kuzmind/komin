package com.komin.core;

public class ApplicationConstants {

    public static int ROOM_MAX_PARTICIPANTS = 20;
    public static int MOST_POPULAR_TAGS_COUNT = 20;
}
