package com.komin.core.exceptions;

public class RoomConnectionException extends KominRunTimeException {
    private final static String exceptionFormat = "Room with name - %s and id - %s unavailable. Please try again later";
    public RoomConnectionException(String sessionId, String roomName, Long roomId) {
        super(sessionId, getExceptionMessage(roomName, roomId));
    }

    public RoomConnectionException(String sessionId, String message) {
        super(sessionId, message);
    }

    private static String getExceptionMessage(String roomName, Long roomId) {
        return String.format(exceptionFormat, roomName, String.valueOf(roomId));
    }
}
