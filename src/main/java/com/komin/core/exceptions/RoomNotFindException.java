package com.komin.core.exceptions;

public class RoomNotFindException extends KominRunTimeException {
    private final static String exceptionFormat = "Can't find room with name - %s and id - %s";

    public RoomNotFindException(String sessionId, String roomName, Long roomId) {
        super(sessionId, getExceptionMessage(roomName, roomId));
    }

    private static String getExceptionMessage(String roomName, Long roomId) {
        return String.format(exceptionFormat, roomName, String.valueOf(roomId));
    }
}
