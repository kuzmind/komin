package com.komin.core.exceptions;

public class PipelineNotFoundException extends KominRunTimeException {
    private static final String messageFormat = "Can't find pipeline for user %s to receive a video!";
    public PipelineNotFoundException(String sessionId) {
        super(sessionId, getExceptionMessage(sessionId));
    }

    private static String getExceptionMessage(String sessionId) {
        return String.format(messageFormat, sessionId);
    }
}
