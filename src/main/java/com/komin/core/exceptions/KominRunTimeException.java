package com.komin.core.exceptions;

public abstract class KominRunTimeException extends RuntimeException {

    private final String sessionId;
    private final String message;

    KominRunTimeException(String sessionId, String message) {
        this.sessionId = sessionId;
        this.message = message;
    }

    public String getSessionId() {
        return sessionId;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
