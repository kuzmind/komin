package com.komin.core.exceptions;

public class KominConnectionException extends KominRunTimeException {

    public KominConnectionException(String sessionId, String message) {
        super(sessionId, message);
    }
}
