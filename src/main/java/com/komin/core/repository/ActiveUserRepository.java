package com.komin.core.repository;

import com.komin.core.model.KominMediaPipeline;
import com.komin.core.model.UserSession;
import com.komin.core.utils.ResolvingTagsFromUserUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class ActiveUserRepository {
    private static final int QUEUE_INITIAL_CAPACITY = 100;
    private final ArrayDeque<UserSession> activeUserSessions = new ArrayDeque<>(QUEUE_INITIAL_CAPACITY);
    private final PipelineController pipelineController = new PipelineController();

    public synchronized UserSession findCandidate(UserSession userSession) {
        if (activeUserSessions.isEmpty()) {
            activeUserSessions.addLast(userSession);
            return null;
        }
        Collection<String> userTags = userSession.getTags();
        if (!userTags.isEmpty()) {
            UserSession candidate = ResolvingTagsFromUserUtils.resolve(activeUserSessions, userTags);
            if (candidate != null) {
                activeUserSessions.remove(candidate);
                return candidate;
            } else {
                activeUserSessions.addLast(userSession);
                return null;
            }
        }
        return activeUserSessions.pollFirst();
    }

    public boolean removeUserFromQueue(UserSession userSession) {
        return activeUserSessions.removeFirstOccurrence(userSession);
    }

    public void registerPipeline(UserSession userSession, KominMediaPipeline pipeline) {
        pipelineController.register(userSession.getSessionId(), pipeline);
    }

    public void removePipeline(UserSession userSession) {
        pipelineController.remove(userSession.getSessionId());
    }

    public KominMediaPipeline getPipeline(UserSession userSession) {
        return pipelineController.get(userSession.getSessionId());
    }

    public boolean existPipeline(UserSession userSession) {
        return pipelineController.exist(userSession.getSessionId());
    }

    private static class PipelineController {
        final ConcurrentHashMap<String, KominMediaPipeline> pipelineBySessionId = new ConcurrentHashMap<>();

        void register(String sessionId, KominMediaPipeline pipeline) {
            pipelineBySessionId.put(sessionId, pipeline);
        }

        boolean exist(String sessionId) {
            return pipelineBySessionId.containsKey(sessionId);
        }

        KominMediaPipeline get(String sessionId) {
            return pipelineBySessionId.get(sessionId);
        }

        KominMediaPipeline remove(String sessionId) {
            return pipelineBySessionId.remove(sessionId);
        }
    }
}
