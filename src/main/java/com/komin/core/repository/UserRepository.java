package com.komin.core.repository;

import com.komin.core.model.UserSession;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Map of users registered in the system. This class has a concurrent hash map to store users, using
 * its name as key in the map.
 */
public class UserRepository {

  private ConcurrentHashMap<String, UserSession> usersBySessionId = new ConcurrentHashMap<>();

  public void register(UserSession user) {
    usersBySessionId.put(user.getSessionId(), user);
  }

  public UserSession getBySessionId(String sessionId) {
    return usersBySessionId.get(sessionId);
  }

  public boolean exists(String sessionId) {
    return usersBySessionId.containsKey(sessionId);
  }

  public int getUserCount() {
    return usersBySessionId.size();
  }

  public UserSession removeBySessionId(String sessionId) {
    final UserSession user = getBySessionId(sessionId);
    if (user != null) {
      usersBySessionId.remove(sessionId);
    }
    return user;
  }

  public Collection<UserSession> getUserSessions() {
    return usersBySessionId.values();
  }
}
