package com.komin.core.repository;

import com.komin.core.model.Room;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class RoomRepository {
    private final ConcurrentHashMap<Long, Room> idToRoomMap = new ConcurrentHashMap<>();
    private List<Room> sortedRooms;

    public Room getRoom(Long roomId) {
        return idToRoomMap.get(roomId);
    }

    public boolean exist(Room room) {
        return exist(room.getRoomId());
    }

    public boolean exist(Long roomId) {
        return idToRoomMap.containsKey(roomId);
    }

    synchronized public void setSortedRoomCollection(List<Room> sortedRooms) {
        this.sortedRooms = sortedRooms;
    }

    public List<Room> getSortedRooms() {
        return sortedRooms;
    }

    public void addRoom(Room room) {
        idToRoomMap.put(room.getRoomId(), room);
    }

    public Room removeRoom(Long roomId) {
        return idToRoomMap.remove(roomId);
    }

    public Collection<Room> getRooms() {
        return idToRoomMap.values();
    }
}
