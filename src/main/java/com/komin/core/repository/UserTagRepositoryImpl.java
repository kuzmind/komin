package com.komin.core.repository;

import com.google.common.annotations.VisibleForTesting;
import com.komin.core.entities.TagImpl;
import com.komin.core.model.UserSession;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static com.komin.core.ApplicationConstants.MOST_POPULAR_TAGS_COUNT;

public class UserTagRepositoryImpl implements UserTagRepository {
    private final Map<String, Set<String>> tagsMap = new ConcurrentHashMap<>();
    private final Set<String> usersWithTags = new HashSet<>();
    private List<TagImpl> mostPopularTags = new ArrayList<>(MOST_POPULAR_TAGS_COUNT);

    public UserTagRepositoryImpl() {}

    @Override
    public Map<String, Set<String>> getTagsMap() {
        return tagsMap;
    }

    public void registerTags(UserSession userSession) {
        Collection<String> tags = userSession.getTags();
        tags.forEach(tag -> {
            Set<String> strings = tagsMap.computeIfAbsent(tag, x -> new HashSet<>());
            strings.add(userSession.getSessionId());
        });
        usersWithTags.add(userSession.getSessionId());
    }

    public Collection<TagImpl> getMostPopularTags() {
        return mostPopularTags;
    }

    public int countOfUsersWithParticularTag(String tag) {
        Set<String> users = tagsMap.get(tag);
        if (users == null) {
            return 0;
        }
        return users.size();
    }

    public void updateInfo(Collection<UserSession> userSessions) {
        synchronized (this) {
            tagsMap.clear();
            usersWithTags.clear();
            userSessions.forEach(this::registerTags);
        }
        sortTags();
    }

    @VisibleForTesting
    public void sortTags() {
        List<Map.Entry<String, Set<String>>> sortedTags = tagsMap.entrySet().parallelStream()
                .sorted(new TagMapComparator()).collect(Collectors.toList());
        mostPopularTags = sortedTags.stream().limit(MOST_POPULAR_TAGS_COUNT)
                .map(stringSetEntry -> new TagImpl(stringSetEntry.getKey(), stringSetEntry.getValue().size()))
                .collect(Collectors.toList());
    }

    public void deleteTags(UserSession userSession) {
        usersWithTags.remove(userSession.getSessionId());
        Collection<String> tags = userSession.getTags();
        tags.forEach(tag -> {
            Set<String> strings = tagsMap.get(tag);
            if (strings != null) {
                strings.remove(userSession.getSessionId());
                if (strings.isEmpty()) {
                    tagsMap.remove(tag);
                }
            }
        });
    }

    public Set<String> getUsersWithTags() {
        return usersWithTags;
    }

    private static class TagMapComparator implements Comparator<Map.Entry<String, Set<String>>> {
        @Override
        public int compare(Map.Entry<String, Set<String>> o1, Map.Entry<String, Set<String>> o2) {
            int firstSize = o1.getValue().size();
            int secondSize = o2.getValue().size();
            return -1 * Integer.compare(firstSize, secondSize);
        }
    }
}
