package com.komin.core.repository;

import com.komin.core.model.UserSession;
import com.komin.core.entities.TagImpl;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface UserTagRepository {

    Map<String, Set<String>> getTagsMap();

    void registerTags(UserSession userSession);

    void deleteTags(UserSession userSession);

    Collection<TagImpl> getMostPopularTags();

    Set<String> getUsersWithTags();

    int countOfUsersWithParticularTag(String tag);

    void updateInfo(Collection<UserSession> userSessions);
}
