package com.komin.core.utils;

import com.komin.core.model.UserSession;
import com.komin.core.repository.UserTagRepository;
import com.komin.core.repository.UserRepository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class TagRepositoryUpdaterThread implements Runnable {

    private final UserTagRepository userTagRepository;
    private final UserRepository userRepository;

    public TagRepositoryUpdaterThread(UserTagRepository userTagRepository, UserRepository userRepository) {
        this.userTagRepository = userTagRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void run() {
        Collection<UserSession> userSessions = userRepository.getUserSessions();
        List<UserSession> usersWithTags = userSessions.stream().filter(userSession -> !userSession.getTags().isEmpty()).collect(Collectors.toList());
        userTagRepository.updateInfo(usersWithTags);
    }
}
