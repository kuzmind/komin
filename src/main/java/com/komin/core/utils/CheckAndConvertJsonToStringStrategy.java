package com.komin.core.utils;

import java.util.Map;

public class CheckAndConvertJsonToStringStrategy {

    public static String checkAndGet(Map objectMap, String jsonParameter) {
        Object o = objectMap.get(jsonParameter);
        String result = String.valueOf(o);
        if (result.equals("") || result.equals("null")) {
            String format = "Can't find property - %s! Please check input parameters!";
            throw new IllegalStateException(String.format(format, jsonParameter));
        }
        return result;
    }
}
