package com.komin.core.utils;

import com.komin.core.model.Room;
import com.komin.core.repository.RoomRepository;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class RoomRepositoryUpdaterThread implements Runnable {

    private RoomRepository roomRepository;

    public RoomRepositoryUpdaterThread(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public void run() {
        Collection<Room> rooms = roomRepository.getRooms();
        List<Room> sortedRooms = rooms.stream().sorted(Comparator.comparingInt(Room::getNumberOfParticipants)).collect(Collectors.toList());
        roomRepository.setSortedRoomCollection(sortedRooms);
    }
}
