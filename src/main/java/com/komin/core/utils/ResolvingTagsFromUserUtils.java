package com.komin.core.utils;

import com.komin.core.model.UserSession;

import java.util.Collection;
import java.util.PriorityQueue;

public class ResolvingTagsFromUserUtils {

    public static UserSession resolve(Collection<UserSession> userSessions, Collection<String> tags) {
        PriorityQueue<Candidate> candidates = new PriorityQueue<>(userSessions.size());
        for (UserSession userSession : userSessions) {
            Collection<String> userTags = userSession.getTags();
            int i = numberOfCoincidence(userTags, tags);
            candidates.add(new Candidate(userSession, i));
        }
        Candidate candidate = candidates.poll();
        if (candidate == null || candidate.getRating() == 0) {
            return null;
        }
        return candidate.getUserSession();
    }

    private static int numberOfCoincidence(Collection<String> first, Collection<String> second) {
        int coincidence = 0;
        for (String s : second) {
            if (first.contains(s)) {
                coincidence++;
            }
        }
        return coincidence;
    }

    private static class Candidate implements Comparable<Candidate> {
        UserSession getUserSession() {
            return userSession;
        }

        int getRating() {
            return rating;
        }

        private final UserSession userSession;
        private final int rating;

        Candidate(UserSession userSession, int rating) {
            this.userSession = userSession;
            this.rating = rating;
        }

        @Override
        public int compareTo(Candidate o) {
            return -1 * Integer.compare(this.rating, o.getRating());
        }
    }
}
