package com.komin.core.configuration;

import com.komin.core.repository.UserTagRepository;
import com.komin.core.repository.UserTagRepositoryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserTagConfiguration {

    @Bean
    public UserTagRepository tagRepository() {
        return new UserTagRepositoryImpl();
    }
}
