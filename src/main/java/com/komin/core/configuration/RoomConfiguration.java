package com.komin.core.configuration;

import com.komin.core.model.RoomFactory;
import com.komin.core.property.GroupCallProperties;
import com.komin.core.services.SendMessageToUserService;
import org.kurento.client.KurentoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RoomConfiguration {

    @Bean
    @Autowired
    public RoomFactory initRoomFactory(KurentoClient kurentoClient, SendMessageToUserService messageToUserService,
                                       GroupCallProperties groupCallProperties) {
        return new RoomFactory(kurentoClient, messageToUserService, groupCallProperties.getMaxParticipants());
    }
}
