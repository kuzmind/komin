package com.komin.core.configuration;

import com.komin.core.repository.UserRepository;
import org.kurento.client.KurentoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

    @Value("${kms.url}")
    private String kmsUrl;

    @Bean
    public UserRepository getUserRepository() {
        return new UserRepository();
    }

    @Bean
    public KurentoClient kurentoClient() {
        return KurentoClient.create(kmsUrl);
    }
}
