package com.komin.core;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.komin.core.model.UserSession;
import com.komin.core.services.SendMessageToUserService;
import org.kurento.client.EventListener;
import org.kurento.client.IceCandidate;
import org.kurento.client.IceCandidateFoundEvent;
import org.kurento.jsonrpc.JsonUtils;

import static com.komin.core.JsonConstants.CANDIDATE;
import static com.komin.core.JsonConstants.ICE_CANDIDATE_ENDPOINT;

public class IceCandidateFoundListener implements EventListener<IceCandidateFoundEvent> {
    private final UserSession userSession;
    private final SendMessageToUserService sendMessageToUserService;
    private final JsonObject jsonObject = new JsonObject();

    public IceCandidateFoundListener(UserSession userSession, SendMessageToUserService sendMessageToUserService) {
        this.userSession = userSession;
        this.sendMessageToUserService = sendMessageToUserService;
    }

    @Override
    public void onEvent(IceCandidateFoundEvent iceCandidateFoundEvent) {
        IceCandidate candidate = iceCandidateFoundEvent.getCandidate();
        sendIceCandidate(userSession, candidate);
    }

    private void sendIceCandidate(UserSession secondUser, IceCandidate candidate) {
        addJSonElementToJSon(CANDIDATE, JsonUtils.toJsonObject(candidate));
        sendMessageToUserService.sendMessage(ICE_CANDIDATE_ENDPOINT, getJsonObject(), secondUser.getSessionId());
    }

    public void addStringPropertyToJSon(String key, String value) {
        jsonObject.addProperty(key, value);
    }

    public void addJSonElementToJSon(String key, JsonElement jsonElement) {
        jsonObject.add(key, jsonElement);
    }

    public JsonObject getJsonObject() {
        return jsonObject;
    }
}
