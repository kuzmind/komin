package com.komin.core;

public final class JsonConstants {
    public static final String SESSION_ID = "sessionId";
    public static final String CANDIDATE = "candidate";
    public static final String CANDIDATES = "candidates";
    public static final String ICE_CANDIDATE = "iceCandidate";
    public static final String SDP_ANSWER = "sdpAnswer";
    public static final String SDP_OFFER = "sdpOffer";
    public static final String ACTION = "action";
    public static final String WAIT = "wait";
    public static final String STOP = "stop";
    public static final String NAME = "name";
    public static final String IS_STREAMER = "isStreamer";
    public static final String PARTICIPANTS = "participants";
    public static final String ROOM_NAME = "roomName";
    public static final String ROOM_ID = "roomId";
    public static final String ROOM_PARTICIPANTS_NUMBER = "roomParticipantsNumber";
    public static final String ROOMS = "rooms";
    public static final String ON_ICE_CANDIDATES = "onIceCandidates";
    public static final String SESSION = "session";
    public static final String SDP_MID = "sdpMid";
    public static final String SDP_M_LINE_INDEX = "sdpMLineIndex";
    public static final String PARTICIPANT_NAME = "participantName";
    public static final String ANONYMOUS = "anonymous";
    public static final String TAG = "tag";
    public static final String TAGS = "tags";
    public static final String COUNT = "count";
    public static final String MOST_POPULAR_TAGS = "mostPopularTags";
    public static final String USERS_WITH_TAGS = "usersWithTags";
    public static final String USERS_COUNT = "usersCount";
    public static final String CURRENT_HOLDER = "currentHolder";
    public static final String IS_ME = "isMe";

    public static final String WAIT_COMMAND_ENDPOINT = "/queue/wait";
    public static final String STOP_COMMAND_ENDPOINT = "/queue/stop";
    public static final String CONNECT_COMMAND_ENDPOINT = "/queue/connect";
    public final static String ICE_CANDIDATES_COMMAND_ENDPOINT = "/queue/iceCandidates";
    public static final String ICE_CANDIDATE_ENDPOINT = "/queue/iceCandidate";
    public final static String APP_INFO_COMMAND_ENDPOINT = "/queue/appInfo";
    public final static String TAG_INFO_COMMAND_ENDPOINT = "/queue/tagInfo";

    public static final String NEW_MESSAGE_ENDPOINT = "/queue/newMessage";

    public static final String GROUP_CALL_NEW_JOINER_ENDPOINT = "/queue/newJoiner";
    public static final String GROUP_CALL_EXISTING_PARTICIPANTS_ENDPOINT = "/queue/existingParticipants";
    public static final String GROUP_CALL_USER_LEFT_ENDPOINT = "/queue/userLeft";
    public static final String GROUP_CALL_CREATE_ROOM_ENDPOINT = "/queue/createRoom";
    public final static String GROUP_CALL_RECEIVE_VIDEO_FROM_ENDPOINT = "/queue/receiveVideoAnswer";
}
