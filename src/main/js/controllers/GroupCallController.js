import GroupCallContentBlock from "../components/GroupCallContentBlock";
import Room from "../components/Room";

const React = require('react');
const axios = require('axios');

require('stompjs');
var socket;
var ws;

export default class GroupCallController extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            destinations : {
                iceCandidate : "/app/iceCandidate",
                createRoom : "/app/createRoom",
                joinRoom : "/app/joinRoom",
                leaveRoom: "/app/leaveRoom",
                sendNewChatMessage: "/app/sendMessage",
                receiveVideoFrom: "/app/receiveVideoFrom"
            },
            isInRoom : false,
            contentBlock : null,
            currentRoom : null,
            roomData : null
        };
        this.showMessage = this.showMessage.bind(this);
        this.showError = this.showError.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.registerWebSockets = this.registerWebSockets.bind(this);
        this.sendAppInfoRequest = this.sendAppInfoRequest.bind(this);
        this.registerContentBlock = this.registerContentBlock.bind(this);
        this.receiveChatMessage = this.receiveChatMessage.bind(this);
        this.receiveAppInfo = this.receiveAppInfo.bind(this);
        this.sendNewChatMessage = this.sendNewChatMessage.bind(this);
        this.createRoomButtonAction = this.createRoomButtonAction.bind(this);
        this.joinRoomButtonAction = this.joinRoomButtonAction.bind(this);
        this.createRoom = this.createRoom.bind(this);
        this.newJoinerArrived = this.newJoinerArrived.bind(this);
        this.receiveIceCandidate = this.receiveIceCandidate.bind(this);
        this.receiveVideoAnswer = this.receiveVideoAnswer.bind(this);
        this.registerRoom = this.registerRoom.bind(this);
        this.userLeft = this.userLeft.bind(this);
        this.initOtherParticipantsInRoom = this.initOtherParticipantsInRoom.bind(this);
        this.disposeRoom = this.disposeRoom.bind(this);
    }

    componentDidMount() {
        socket = new WebSocket("wss://" + location.host + "/actions");
        ws = Stomp.over(socket);
        this.registerWebSockets([
            {route: '/user/queue/errors', callback: this.showError},
            {route: '/user/queue/iceCandidate', callback: this.receiveIceCandidate},
            {route: '/user/queue/receiveVideoAnswer', callback: this.receiveVideoAnswer},
            {route: '/user/queue/newJoiner', callback: this.newJoinerArrived},
            {route: '/user/queue/createRoom', callback: this.createRoom},
            {route: '/user/queue/existingParticipants', callback: this.initOtherParticipantsInRoom},
            {route: '/user/queue/userLeft', callback: this.userLeft},
            {route: '/user/queue/newMessage', callback: this.receiveChatMessage},
        ]);
    }

    componentWillUnmount() {
        console.log("Closing ws connection");
        ws.disconnect();
    }

    registerWebSockets(registrations) {
        ws.connect({}, function(frame) {
            registrations.forEach(function (registration) {
                ws.subscribe(registration.route, registration.callback);
            });
        }, function(error) {
            alert("STOMP error " + error);
        });
    }

    registerRoom(room) {
        this.setState({currentRoom : room})
    }

    createRoom(message) {
        if (message.body) {
            let jsonMessage = JSON.parse(message.body);
            let roomName = jsonMessage.roomName;
            let id = jsonMessage.roomId;
            let currentHolder = jsonMessage.currentHolder;
            let participants = jsonMessage.participants;
            let roomData = {
                participants : participants,
                id : id,
                name : roomName,
                currentHolder : currentHolder
            };
            this.setState({
                isInRoom : true,
                roomData : roomData
            })
        }
    }

    disposeRoom() {
        const message = {
            action: "leaveRoom",
            roomName: this.state.roomData.name,
            roomId: this.state.roomData.id
        };
        this.sendMessage(this.state.destinations.leaveRoom, message);
        this.setState({
            isInRoom : false,
            currentRoom : null
        });
    }


    newJoinerArrived(message) {
        if (message.body) {
            if (this.state.isInRoom === true && this.state.currentRoom != null) {
                let jsonMessage = JSON.parse(message.body);
                let newParticipant = {
                    sessionId : jsonMessage.sessionId,
                    name : jsonMessage.name,
                    isStreamer : jsonMessage.isStreamer
                };
                this.state.currentRoom.newJoinerArrived(newParticipant);
            }
        }
    }

    initOtherParticipantsInRoom(message) {
        if (message.body) {
            if (this.state.isInRoom === true && this.state.currentRoom != null) {
                let jsonMessage = JSON.parse(message.body);
                let participants = jsonMessage.participants;
                this.state.currentRoom.initOtherParticipants(participants);
            }
        }
    }

    receiveVideoAnswer(message) {
        if (message.body) {
            if (this.state.isInRoom === true && this.state.currentRoom != null) {
                let jsonMessage = JSON.parse(message.body);
                let sessionId = jsonMessage.sessionId;
                let sdpAnswer = jsonMessage.sdpAnswer;
                this.state.currentRoom.receiveVideoAnswerForUser(sessionId, sdpAnswer);
            }
        }
    }

    receiveIceCandidate(message) {
        if (message.body) {
            if (this.state.isInRoom === true && this.state.currentRoom != null) {
                const parsedMessage = JSON.parse(message.body);
                let sessionId = parsedMessage.sessionId;
                let candidate = parsedMessage.candidate;
                this.state.currentRoom.addIceCandidateToUser(sessionId, candidate);
            }
        }
    }

    userLeft(message) {
        if (message.body) {
            if (this.state.isInRoom === true && this.state.currentRoom != null) {
                var parsedMessage = JSON.parse(message.body);
                let sessionId = parsedMessage.sessionId;
                this.state.currentRoom.deleteParticipant(sessionId);
            }
        }
    }

    registerContentBlock(object) {
        this.setState({contentBlock : object});
    }

    showMessage(message) {
        console.log("Received message from server - " + message);
        alert(message.body);
    }

    receiveChatMessage(message) {
        this.state.currentRoom.state.chat.addExternalMessage(message.body);
    }

    receiveAppInfo(message) {
        this.state.contentBlock.state.tags.updateInfo(message.body);
    }

    sendNewChatMessage(message) {
        this.sendMessage(this.state.destinations.sendNewChatMessage, message);
    }

    sendAppInfoRequest() {
        var message = {action : "appInfo"};
        this.sendMessage(this.state.destinations.getAppInfo, message);
    }

    showError(message) {
        console.log("Error - " + message.body);
        alert("Error " + message.body);
    }

    sendMessage(destination, message) {
        console.log("Sending message to server - " + message);
        ws.send(destination, {}, JSON.stringify(message));
    }

    createRoomButtonAction() {
        let roomName = this.state.contentBlock.state.createRoomForm.state.roomName;
        let participantName = this.state.contentBlock.state.createRoomForm.state.participantName;
        let message = {roomName : roomName, participantName : participantName};
        this.sendMessage(this.state.destinations.createRoom, message)
    }

    joinRoomButtonAction() {
        let roomName = this.state.contentBlock.state.joinRoomForm.state.roomName;
        let participantName = this.state.contentBlock.state.joinRoomForm.state.participantName;
        let message = {roomName : roomName, roomId : roomName, participantName : participantName};
        this.sendMessage(this.state.destinations.joinRoom, message);
    }

    render() {
        let content;
        if (this.state.isInRoom) {
            content = <Room participants={this.state.roomData.participants}
                            id={this.state.roomData.id}
                            name={this.state.roomData.name}
                            currentHolder={this.state.roomData.currentHolder}
                            websocket={ws}
                            register={this.registerRoom}
                            onLeaveAction={this.disposeRoom}
                            chatButtonAction={this.sendNewChatMessage}
            />;
        } else {
            content = <GroupCallContentBlock register={this.registerContentBlock}
                                             createRoomButtonAction={this.createRoomButtonAction}
                                             joinRoomButtonAction={this.joinRoomButtonAction}/>
        }
        return (
            <div>
                {content}
            </div>
        )
    }
}