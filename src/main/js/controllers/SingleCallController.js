const React = require('react');
const axios = require('axios');
import One2OneContentBlock from '../components/One2OneContentBlock';

require('stompjs');
var socket;
var ws;
var webRtcPeer;

export default class SingleCallController extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            destinations : {
                sdpOffer : "/app/sdpOffer",
                iceCandidates : "/app/iceCandidates",
                stop : "/app/stop",
                deleteMe : "/app/deleteUser",
                sendNewChatMessage: "/app/sendMessage",
                getAppInfo: "/app/getAppInfo",
                getTagInfo: "/app/getTagInfo"
            },
            videoInput : null,
            videoOutput : null,
            startButtonDescription: {
                call : "Call" ,
                next: "Next"
            },
            mediaConstraints : {
                audio: true,
                video: {
                    width: 640,
                    framerate: 15
                }
            },
            offerSdp : null,
            candidates: [],
            contentBlock : null,
            anonymous : false
        };
        this.showMessage = this.showMessage.bind(this);
        this.showError = this.showError.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.registerWebSockets = this.registerWebSockets.bind(this);
        this.registerTabCloseAndRefreshEvent = this.registerTabCloseAndRefreshEvent.bind(this);
        this.sendOfferAndIceCandidate = this.sendOfferAndIceCandidate.bind(this);
        this.generateOffer = this.generateOffer.bind(this);
        this.onIceCandidate = this.onIceCandidate.bind(this);
        this.sendrecvCallBack = this.sendrecvCallBack.bind(this);
        this.invokeCallRequest = this.invokeCallRequest.bind(this);
        this.sendIceCandidates = this.sendIceCandidates.bind(this);
        this.receiveIceCandidate = this.receiveIceCandidate.bind(this);
        this.sendTagInfoRequest = this.sendTagInfoRequest.bind(this);
        this.sendAppInfoRequest = this.sendAppInfoRequest.bind(this);
        this.registerContentBlock = this.registerContentBlock.bind(this);
        this.wait = this.wait.bind(this);
        this.stopCommunication = this.stopCommunication.bind(this);
        this.connectUsers = this.connectUsers.bind(this);
        this.findNext = this.findNext.bind(this);
        this.receiveChatMessage = this.receiveChatMessage.bind(this);
        this.receiveAppInfo = this.receiveAppInfo.bind(this);
        this.receiveTagInfo = this.receiveTagInfo.bind(this);
        this.sendNewChatMessage = this.sendNewChatMessage.bind(this);
        this.checkBoxOnChange = this.checkBoxOnChange.bind(this);
        this.updateAppInfo = this.updateAppInfo.bind(this);
    }

    componentDidMount() {
        socket = new WebSocket("wss://" + location.host + "/actions");
        ws = Stomp.over(socket);
        this.registerWebSockets([
            {route: '/user/queue/errors', callback: this.showError},
            {route: '/user/queue/wait', callback: this.wait},
            {route: '/user/queue/connect', callback: this.connectUsers},
            {route: '/user/queue/stop', callback: this.findNext},
            {route: '/user/queue/iceCandidates', callback: this.sendIceCandidates},
            {route: '/user/queue/iceCandidate', callback: this.receiveIceCandidate},
            {route: '/user/queue/newMessage', callback: this.receiveChatMessage},
            {route: '/user/queue/tagInfo', callback: this.receiveTagInfo},
        ]);
        this.setState({videoInput : document.getElementById('videoInput'),
            videoOutput : document.getElementById('videoOutput')});
        this.updateAppInfo();
        this.timerID = setInterval(
            () => this.updateAppInfo(),
            10000
        );

    }

    updateAppInfo() {
        axios.get('https://' + location.host + '/appInfo')
            .then(response => {
                    console.log(response.data);
                    let message = {
                        body: response.data
                    };
                    this.receiveAppInfo(message);
                }
            );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
        console.log("Closing ws connection");
        ws.disconnect();
    }

    registerTabCloseAndRefreshEvent() {
        window.onbeforeunload = (e) => {
            console.log("Sending message to delete Me from active users");
            var message = {action : "deleteUser"};
            this.sendMessage(this.state.destinations.deleteMe, message);
        };
        window.onbeforeunload = (e) => {
            var message = {action : "deleteUser"};
            this.sendMessage(this.state.destinations.deleteMe, message);
        }
    }

    registerWebSockets(registrations) {
        ws.connect({}, function(frame) {
            registrations.forEach(function (registration) {
                ws.subscribe(registration.route, registration.callback);
            });
        }, function(error) {
            alert("STOMP error " + error);
        });
    }

    checkBoxOnChange(event) {
        this.setState({anonymous: !this.state.anonymous});
    }

    registerContentBlock(object) {
        this.setState({contentBlock : object});
    }

    showMessage(message) {
        console.log("Received message from server - " + message);
        alert(message.body);
    }

    receiveChatMessage(message) {
        this.state.contentBlock.state.chat.addExternalMessage(message.body);
    }

    receiveAppInfo(message) {
        this.state.contentBlock.state.tags.updateInfo(message.body);
    }

    receiveTagInfo(message) {
        this.state.contentBlock.state.tags.incomeTagInfo(message.body);
    }

    sendNewChatMessage(message) {
        this.sendMessage(this.state.destinations.sendNewChatMessage, message);
    }

    sendTagInfoRequest(message) {
        var resultMessage = {tag : message};
        this.sendMessage(this.state.destinations.getTagInfo, resultMessage);
    }

    /*async */sendAppInfoRequest() {
        var message = {action : "appInfo"};
        // var result = await new Promise(resolve => {
        this.sendMessage(this.state.destinations.getAppInfo, message);
        // resolve('resolved');
        // });
    }

    showError(message) {
        console.log("Error - " + message.body);
        alert("Error " + message.body);
    }

    wait(message) {
        console.log("Received wait command");
        this.state.contentBlock.state.startButton.setState({enable : false});
        this.state.contentBlock.setState({wait : true});
    }

    connectUsers(message) {
        var parsedMessage = JSON.parse(message.body);
        console.log("Received connect command with message - " + parsedMessage);
        webRtcPeer.processAnswer(parsedMessage.sdpAnswer, function(error) {
            if (error)
                return console.error(error);
        });
        this.state.contentBlock.state.startButton.setState({
            enable : true,
            description : this.state.startButtonDescription.next,
            bindAction: this.findNext
        });
        this.state.contentBlock.state.stopButton.setState({enable : true});
        this.state.contentBlock.setState({wait : false});
        this.state.contentBlock.state.chat.refresh();
        this.state.contentBlock.state.chat.setState({enable : true});
    }

    findNext() {
        this.stopCommunication();
        this.invokeCallRequest();
    }

    receiveIceCandidate(message) {
        var parsedMessage = JSON.parse(message.body);
        console.log("Received iceCandidate - " + parsedMessage);
        webRtcPeer.addIceCandidate(parsedMessage.candidate, function(error) {
            if (error)
                return console.error('Error adding candidate: ' + error);
        });
    }

    sendIceCandidates(message) {
        let candidates = this.state.candidates;
        if (candidates !== null && candidates.length > 0) {
            message = {
                candidates : candidates,
                anonymous : this.state.anonymous
            };
            this.sendMessage(this.state.destinations.iceCandidates, message);
        } else {
            console.log("Error! Can't find candidates!");
            message = {candidates : "Error! Can't find candidates!"}
        }
    }

    generateOffer(error, offerSdp) {
        if (error) {
            return console.error(error);
        }
        this.setState({offerSdp : offerSdp});
        let message = {
            action : "sdpOffer",
            sdpOffer : offerSdp,
            tags: this.state.contentBlock.state.tags.state.myTags
        };
        this.sendMessage(this.state.destinations.sdpOffer, message);
    }

    onIceCandidate(candidate) {
        console.info("IceCandidate : " + candidate);
        this.setState(prevState => ({
            candidates: [...prevState.candidates, candidate]
        }));
    }

    sendrecvCallBack(error) {
        if (error) {
            return console.error(error);
        }

        webRtcPeer.generateOffer(this.generateOffer.bind(this));
    }

    invokeCallRequest(object) {
        this.sendOfferAndIceCandidate();
        this.state.contentBlock.state.stopButton.setState({enable : true});
    }

    sendOfferAndIceCandidate() {
        var options = {
            localVideo : this.state.videoInput,
            remoteVideo : this.state.videoOutput,
            onicecandidate : this.onIceCandidate,
            onerror : this.showError,
            mediaConstraints : this.state.mediaConstraints
        };
        webRtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerSendrecv(options, this.sendrecvCallBack);
    }

    stopCommunication() {
        console.log("Received stop command");
        if (webRtcPeer) {
            webRtcPeer.dispose();
            webRtcPeer = null;

            var message = {action : "stop"};
            this.sendMessage(this.state.destinations.stop, message);
        }

        this.state.contentBlock.state.startButton.setState({
            enable : true,
            description : this.state.startButtonDescription.call,
            bindAction : this.invokeCallRequest
        });
        this.state.contentBlock.state.stopButton.setState({enable : false});
        this.state.contentBlock.state.chat.setState({enable : false});
        this.state.contentBlock.setState({wait : false});
    }

    sendMessage(destination, message) {
        console.log("Sending message to server - " + message);
        ws.send(destination, {}, JSON.stringify(message));
    }

    render() {
        return (
            <div>
                <One2OneContentBlock startButtonAction={this.invokeCallRequest}
                                     startButtonDescription={this.state.startButtonDescription.call}
                                     stopButtonAction={this.stopCommunication}
                                     chatButtonAction={this.sendNewChatMessage}
                                     checkBoxOnChange={this.checkBoxOnChange}
                                     register={this.registerContentBlock}
                                     sendTagInfoRequest={this.sendTagInfoRequest}
                />
            </div>
        )
    }
}