const React = require('react');
const ReactDOM = require('react-dom');

import CallRouter from './routing/CallRouter';


class App extends React.Component {

    render() {
        return (
            <div>
                {CallRouter()}
            </div>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('react'));