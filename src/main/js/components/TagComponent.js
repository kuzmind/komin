const React = require('react');

export default class TagComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            usersCount: 0,
            usersWithTags: 0,
            mostPopularTags: [],
            currentTag: '',
            myTags: [],
            maxTags: 5,
            register: props.register,
            sendAction: props.sendAction
        };
        this.popularTagClick = this.popularTagClick.bind(this);
        this.addMyTag = this.addMyTag.bind(this);
        this.deleteMyTag = this.deleteMyTag.bind(this);
        this.updateInfo = this.updateInfo.bind(this);
        this.requestInfoAboutTag = this.requestInfoAboutTag.bind(this);
        this.incomeTagInfo = this.incomeTagInfo.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
    }

    componentDidMount() {
        this.state.register(this);
    }

    popularTagClick(tag) {
        if (this.state.myTags.length < this.state.maxTags) {
            this.addMyTag(tag);
        }
    }

    addMyTag(tag) {
        let name;
        let count;
        if (tag instanceof Tag) {
            name = tag.state.tag;
            count = tag.state.count;
        } else {
            name = tag.tag;
            count = tag.count;
        }
        const {myTags} = this.state;
        const newTag = {
            tag: name,
            count: count
        };
        const newTags = [...myTags, newTag];
        this.setState({myTags: newTags});
    }

    deleteMyTag(tag) {
        const {myTags} = this.state;
        var tagToDelete = {
            tag: tag.state.name,
            count: tag.state.count
        };
        let indexOf = myTags.indexOf(tagToDelete);
        const newTags = myTags.splice(indexOf, 1);
        this.setState({myTags, newTags});
    }

    updateInfo(message) {
        if (message) {
            const parsedMessage = message;
            let userCount = parsedMessage.usersCount;
            let usersWithTags = parsedMessage.usersWithTags;
            let mostPopularTags = parsedMessage.mostPopularTags;
            let newTags = [];
            const {myTags} = this.state;
            mostPopularTags.forEach(item => {
                const newTag = {
                    tag: item.tag,
                    count: item.count
                };
                newTags = [...newTags, newTag];
                myTags.forEach(myTag => {
                    if (myTag.tag.toLowerCase() === item.tag.toLowerCase()) {
                        myTag.count = item.count;
                    }
                });
            });
            this.setState({
                usersCount: userCount,
                usersWithTags: usersWithTags,
                mostPopularTags: newTags,
                myTags: myTags
            });
        }
    }

    requestInfoAboutTag() {
        if (this.state.currentTag) {
            let tagWithoutSpaces = this.state.currentTag.replace(/\s/g, '');
            this.state.sendAction(tagWithoutSpaces);
        }
    }

    incomeTagInfo(message) {
        if (message) {
            if (this.state.myTags.length >= this.state.maxTags) {
                return;
            }
            const parsedMessage = JSON.parse(message);
            let tag = parsedMessage.tag;
            let count = parsedMessage.count;
            const {myTags} = this.state;
            let wasUpdate = false;
            document.getElementById("tagInput").value = '';
            myTags.forEach(myTag => {
                if (tag.toLowerCase() === myTag.tag.toLowerCase()) {
                    myTag.count = count;
                    this.setState({
                        myTags: myTags,
                    });
                    wasUpdate = true;
                }
            });
            if (wasUpdate === true) {
                return;
            }
            const newTag = {
                tag: tag,
                count: count
            };
            this.addMyTag(newTag);
        }
    }

    clearTags() {
        document.getElementById("tagInput").value = '';
        this.setState({
            myTags: [],
            currentTag: ''
        })
    }

    onInputChange(e) {
        this.setState({currentTag: e.target.value});
    }

    render() {
        var popularTags = this.state.mostPopularTags.map(item => {
            return <PopularTag key={item.tag} tag={item.tag} count={item.count} onClick={this.popularTagClick}/>
        });
        var myTags = this.state.myTags.map(item => {
            return <MyTag key={item.tag} tag={item.tag} count={item.count} onClick={this.deleteMyTag}/>
        });

        return (
            <div>
                <div className="appInfo col-md-12">
                    <p>Active users : {this.state.usersCount}</p>
                    <p>Active users with tags: {this.state.usersWithTags}</p>
                </div>
                <div className="popularTags col-md-12">
                    {popularTags}
                </div>
                <div className="col-md-12">
                    <input id="tagInput" onChange={this.onInputChange} pattern="[a-zA-Z0-9_&-]+"/>
                    <button id="sendTag" onClick={this.requestInfoAboutTag}>
                        <img src="img/search.png"/>
                    </button>
                </div>
                <div className="myTags col-md-12">
                    {myTags}
                </div>
            </div>
        )
    }
}

class Tag extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tag: props.tag,
            count: props.count,
            onClick: props.onClick
        }
    }
}

class MyTag extends Tag {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick(e) {
        this.state.onClick(this);
    }

    render() {
        return (
            <div className="myTag">
                <div className="myTagContainer">
                    {this.state.tag}
                    <button type="button" aria-label="Close" className="top-right deleteTagButton" onClick={this.onClick}>
                        <span aria-hidden="true">x</span>
                    </button>
                    <p className="countLabel">{this.state.count}</p>
                </div>
            </div>
        )
    }
}

class PopularTag extends Tag {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick(e) {
        this.state.onClick(this);
    }

    render() {
        return (
            <div className="popularTag" onClick={this.onClick}>
                <p>{this.state.tag}</p>
                <p>{this.state.count}</p>
            </div>
        )
    }
}