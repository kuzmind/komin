const React = require('react');

export default class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            newItem : '',
            items : [],
            enable : props.enable,
            internalAvatar : 'img/batman.png',
            externalAvatar : 'img/orc.png',
            users : [],
            myInfo : props.chatHolder,
            register : props.register,
            sendButtonAction : props.chatButtonAction,
        };

        this.formatAMPM = this.formatAMPM.bind(this);
        this.addExternalMessage = this.addExternalMessage.bind(this);
        this.addInternalMessage = this.addInternalMessage.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
        this.refresh = this.refresh.bind(this);
        this.scrollToBottom = this.scrollToBottom.bind(this);
        this.deleteParticipant = this.deleteParticipant.bind(this);
        this.addParticipant = this.addParticipant.bind(this);
        this.getUserNameById = this.getUserNameById.bind(this);
    }

    componentDidMount() {
        if (this.state.register) {
            this.state.register(this);
        }
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    scrollToBottom() {
        this.messagesEnd.scrollIntoView({behavior: "smooth"});
    }

    formatAMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0' + minutes : minutes;
        return hours + ':' + minutes + ' ' + ampm;
    }

    getUserNameById(userId) {
        if (!userId) {
            return "";
        }
        let participantToFind = this.state.users.find(participant => participant.id === userId);
        if (participantToFind) {
            return participantToFind.name;
        }
        return "";
    }

    addExternalMessage(message) {
        if (message) {
            var parsedMessage = JSON.parse(message);
            var items = this.state.items;
            var date = this.formatAMPM(new Date());
            let userName = this.getUserNameById(parsedMessage.sessionId);
            var newMessage = {
                value: parsedMessage.newMessage,
                date: date,
                userName : userName,
                internal: false
            };
            const nextItem = [...items, newMessage];
            this.setState({items: nextItem});
        }
    }

    addInternalMessage() {
        const {newItem, items} = this.state;
        if (newItem) {
            var date = this.formatAMPM(new Date());
            let name = "";
            if (this.state.myInfo) {
                name = this.state.myInfo.name;
            }
            var newMessage = {
                value: newItem,
                date: date,
                userName : name,
                internal: true
            };
            const nextItem = [...items, newMessage];
            this.setState({items: nextItem, newItem: ''});

            var newChatMessage = {newMessage: newItem};
            this.state.sendButtonAction(newChatMessage);
        }
    }

    onChange(e) {
        this.setState({newItem: e.target.value});
    }

    onKeyPress(e) {
        if (e.key === 'Enter') {
            document.getElementById('sendMessage').click();
        }
    }

    addParticipant(participant) {
        if (participant) {
            this.state.users = [...this.state.users, participant];
        }
    }

    deleteParticipant(participant) {
        this.state.users = participants.filter(function (value, index, array) {
            return value.id !== participant.id;
        });
    }

    refresh() {
        this.setState({items: []})
    }

    render() {
        let iter = 0;
        const items = this.state.items.map(item => {
            iter++;
            return item.internal === true ?
                <ListItemInternal value={item.value}
                                  date={item.date}
                                  userName={item.userName}
                                  avatar={this.state.internalAvatar}
                                  key={iter}
                />
                : <ListItemExternal value={item.value}
                                    date={item.date}
                                    userName={item.userName}
                                    avatar={this.state.externalAvatar}
                                    key={iter}
                />
        });
        return (
            <div>
                <div className="chat">
                    <div className="col-md12 frame">
                        <ul className="chat-content">
                            {items}
                            <div style={{float: "left", clear: "both"}}
                                 ref={(el) => {
                                     this.messagesEnd = el;
                                 }}>
                            </div>
                        </ul>
                        <div>
                            <div className="msj-rta macro" style={{marginBottom: "10px"}}>
                                <div className="text text-r" style={{background: "whitesmoke !important"}}>
                                    <input className="my-text"
                                           value={this.state.newItem}
                                           onChange={this.onChange}
                                           placeholder="Type a message"
                                           onKeyPress={this.onKeyPress}
                                    />
                                </div>
                            </div>
                            <div style={{paddingRight : "16px"}}>
                                <button id="sendMessage" disabled={!this.state.enable} onClick={this.addInternalMessage}>
                                    <img src="img/send.png"/>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class ListItemInternal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value,
            date: props.date,
            userName: props.userName,
            avatar: props.avatar
        }
    }

    render() {
        return (
            <li style={{width: "100%"}}>
                <div className="msj macro">
                    <div className="avatar">
                        <img className="img-circle" style={{width: "100%"}} src={this.state.avatar}/>
                        <span>{this.state.userName}</span>
                    </div>
                    <div className="text text-l">
                        <p>{this.state.value}</p>
                        <p>
                            <small>{this.state.date}</small>
                        </p>
                    </div>
                </div>
            </li>
        )
    }
}

class ListItemExternal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: props.value,
            date: props.date,
            userName: props.userName,
            avatar: props.avatar
        }
    }

    render() {
        return (
            <li style={{width: "100%"}}>
                <div className="msj-rta macro">
                    <div className="text text-r">
                        <p>{this.state.value}</p>
                        <p><span>{this.state.userName}</span></p>
                        <p>
                            <small>{this.state.date}</small>
                        </p>
                    </div>
                    <div className="avatar" style={{padding: "0px 0px 0px 10px !important"}}>
                        <img className="img-circle" style={{width: "100%"}} src={this.state.avatar}/>
                    </div>
                </div>
            </li>
        )
    }
}