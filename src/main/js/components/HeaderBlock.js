import {Link} from "react-router-dom";

const React = require('react');

export default class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <header>
                <div className="navbar navbar-inverse navbar-fixed-top">
                    <div className="container">
                        <div className="navbar-header col-md-12">
                            <ul id="header-links">
                                <li className="col-md-4">
                                    <Link to="/">Random person</Link>
                                </li>
                                <li className="col-md-4">
                                    <Link to="/groups">Group calls</Link>
                                </li>
                                <li className="col-md-4">
                                    <Link to="/about">About</Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}