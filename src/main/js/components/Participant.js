const React = require('react');

export default class Participant extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sessionId : props.sessionId,
            name : props.name,
            isMe : props.isMe,
            rtcPeer : null,
            roomId : props.roomId,
            ws : props.websocket,
            isStreamer : props.isStreamer,
            destinations : {
                iceCandidate : "/app/iceCandidate",
                receiveVideoFrom  : "/app/receiveVideoFrom",
                translateVideoFrom  : "/app/translateVideoFrom",
                sendMeOtherParticipants  : "/app/sendMeOtherParticipants"
            },
            constraints : {
                audio : true,
                video : {
                    mandatory : {
                        maxWidth : 320,
                        maxFrameRate : 15,
                        minFrameRate : 15
                    }
                }
            }
        };
        this.startTranslation = this.startTranslation.bind(this);
        this.startReceiveVideo = this.startReceiveVideo.bind(this);
        this.onIceCandidate = this.onIceCandidate.bind(this);
        this.getVideoElement = this.getVideoElement.bind(this);
        this.offerToReceiveVideoForMe = this.offerToReceiveVideoForMe.bind(this);
        this.offerToReceiveVideoForOther = this.offerToReceiveVideoForOther.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
        this.receiveIceCandidate = this.receiveIceCandidate.bind(this);
        this.receiveSdpAnswer = this.receiveSdpAnswer.bind(this);
        this.dispose = this.dispose.bind(this);
        this.sendMeOtherParticipants = this.sendMeOtherParticipants.bind(this);
    }

    componentDidMount() {
        this.props.register(this);
        if (this.state.isStreamer) {
            if (this.state.isMe) {
                this.startTranslation();
            } else {
                this.startReceiveVideo();
            }
        }
    }

    getVideoElement() {
        let videoId = "video" + this.state.sessionId;
        return document.getElementById(videoId);
    }

    offerToReceiveVideoForMe(error, offerSdp) {
        if (error) return console.error ("sdp offer error");
        console.log('Invoking SDP offer callback function');
        const msg = {
            name: this.state.name,
            sessionId: this.state.sessionId,
            sdpOffer: offerSdp
        };
        let receiveVideoFromDestination = this.state.destinations.translateVideoFrom;
        this.sendMessage(receiveVideoFromDestination, msg);
    }

    offerToReceiveVideoForOther(error, offerSdp) {
        if (error) return console.error ("sdp offer error");
        console.log('Invoking SDP offer callback function');
        const msg = {
            name: this.state.name,
            sessionId: this.state.sessionId,
            sdpOffer: offerSdp
        };
        let receiveVideoFromDestination = this.state.destinations.receiveVideoFrom;
        this.sendMessage(receiveVideoFromDestination, msg);
    }

    startReceiveVideo() {
        let videoId = "video" + this.state.sessionId;
        let remoteVideo = document.getElementById(videoId);
        const options = {
            remoteVideo: remoteVideo,
            mediaConstraints: this.state.constraints,
            onicecandidate: this.onIceCandidate
        };
        let offerToReceiveVideoCallBack = this.offerToReceiveVideoForOther;
        let rtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerRecvonly(options,
            function (error) {
                if(error) {
                    return console.error(error);
                }
                this.generateOffer(offerToReceiveVideoCallBack);
            });
        this.setState({rtcPeer : rtcPeer});
    }

    startTranslation() {
        let videoId = "video" + this.state.sessionId;
        let outgoingVideo = document.getElementById(videoId);
        const options = {
            localVideo: outgoingVideo,
            mediaConstraints: this.state.constraints,
            onicecandidate: this.onIceCandidate
        };
        let offerToReceiveVideoCallBack = this.offerToReceiveVideoForMe;
        let rtcPeer = new kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options,
            function (error) {
                if(error) {
                    return console.error(error);
                }
                this.generateOffer(offerToReceiveVideoCallBack);
            });
        this.setState({rtcPeer : rtcPeer});
    }

    onIceCandidate(candidate) {
        console.info("IceCandidate : " + candidate);
        const message = {
            iceCandidate: candidate,
            name: this.state.name,
            sessionId: this.state.sessionId
        };
        let iceCandidateDestination = this.state.destinations.iceCandidate;
        this.sendMessage(iceCandidateDestination, message);
    }

    receiveIceCandidate(candidate) {
        console.log("Received iceCandidate - " + candidate);
        this.state.rtcPeer.addIceCandidate(candidate, function(error) {
            if (error)
                return console.error('Error adding candidate: ' + error);
        });
    }

    receiveSdpAnswer(sdpAnswer) {
        console.log("Received sdp answer - " + sdpAnswer);
        this.state.rtcPeer.processAnswer(sdpAnswer, this.sendMeOtherParticipants);
    }

    sendMeOtherParticipants() {
        if (this.state.isMe) {
            const message = {roomId : this.state.roomId + ""};
            this.sendMessage(this.state.destinations.sendMeOtherParticipants, message);
        }
    }

    dispose() {
        console.log('Disposing participant ' + this.state.name);
        this.state.rtcPeer.dispose()
    }

    sendMessage(destination, message) {
        console.log("Sending message to server - " + message);
        this.state.ws.send(destination, {}, JSON.stringify(message));
    }

    render() {
        let videoId = "video" + this.state.sessionId;
        let classForVideo = this.state.isMe === true ? "myVideo" : "externalVideo";
        return(
            <div>
                <video id={videoId} className={classForVideo} autoPlay/>
                <p>{this.state.name}</p>
            </div>
        )
    }
}