import CreateRoomForm from "./CreateRoomForm";

const React = require('react');

export default class GroupCallContentBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            createRoomForm : null,
            joinRoomForm : null,
        };
        this.registerCreateRoomForm = this.registerCreateRoomForm.bind(this);
        this.registerJoinRoomForm = this.registerJoinRoomForm.bind(this);
    }

    componentDidMount() {
        this.props.register(this);
    }

    registerCreateRoomForm(object) {
        this.setState({createRoomForm: object});
    }

    registerJoinRoomForm(object) {
        this.setState({joinRoomForm: object});
    }

    render() {
        return (
            <div>
                <div className="container" style={{marginTop: "30px"}}>
                    <div className="col-md-6">
                        <CreateRoomForm createRoomButtonAction={this.props.createRoomButtonAction}
                                        register={this.registerCreateRoomForm}
                                        description="Create Room"/>
                    </div>

                    <div className="col-md-6">
                        <CreateRoomForm createRoomButtonAction={this.props.joinRoomButtonAction}
                                        register={this.registerJoinRoomForm}
                                        description="Join Room"/>
                    </div>
                </div>
            </div>
        )
    }
}