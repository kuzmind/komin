const React = require('react');

export default class Footer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <footer>
                <div>
                    <div className="container text-center">
                        <hr/>
                        <div className="row">&copy; BestGalera</div>
                        <div className="row">For contacts: komin@gmail.com</div>
                    </div>
                </div>
            </footer>
        )
    }
}