import Button from './Button';
const React = require('react');

export default class CreateRoomForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            roomName : '',
            participantName : '',
            createRoomButton : null,
            register: props.register
        };

        this.onRoomNameInputChange = this.onRoomNameInputChange.bind(this);
        this.onParticipantNameInputChange = this.onParticipantNameInputChange.bind(this);
        this.registerCreateRoomButton = this.registerCreateRoomButton.bind(this);
    }

    componentDidMount() {
        this.state.register(this);
    }

    onRoomNameInputChange(e) {
        this.setState({roomName: e.target.value});
    }

    onParticipantNameInputChange(e) {
        this.setState({participantName: e.target.value});
    }

    registerCreateRoomButton(object) {
        this.setState({createRoomButton: object});
    }

    render() {
        return (
            <div className="col-md-12">
                <input className="row" id="roomNameInput" onChange={this.onRoomNameInputChange} pattern="[a-zA-Z0-9_&-]+"/>
                <input className="row" id="participantNameInput" onChange={this.onParticipantNameInputChange} pattern="[a-zA-Z0-9_&-]+"/>
                <Button id="createRoom" class="row"
                        action={this.props.createRoomButtonAction}
                        enable="true"
                        description={this.props.description}
                        register={this.registerCreateRoomButton}
                />
            </div>
        )
    }
}