import Chat from "./ChatComponent";
import Button from './Button';
import TagsComponent from './TagComponent';

const React = require('react');
const Draggabilly = require('draggabilly');

export default class One2OneContentBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            wait: false,
            startButton: null,
            stopButton: null,
            chat: null,
            tags: null,
            register: props.register
        };

        this.registerStartButton = this.registerStartButton.bind(this);
        this.registerStopButton = this.registerStopButton.bind(this);
        this.registerChat = this.registerChat.bind(this);
        this.registerTags = this.registerTags.bind(this);
    }

    componentDidMount() {
        var drag = new Draggabilly(document.getElementById('videoSmall'));
        this.state.register(this);
    }

    registerStartButton(object) {
        this.setState({startButton: object});
    }

    registerStopButton(object) {
        this.setState({stopButton: object});
    }

    registerChat(object) {
        this.setState({chat: object});
    }

    registerTags(object) {
        this.setState({tags: object});
    }

    render() {
        var checkBox =
            <div>
                <input className="center-block" type="checkbox" defaultChecked={false}
                    onChange={this.props.checkBoxOnChange}/>Hide face?
            </div>;
        var startButton = <Button class="btn btn-success center-block" id="call"
                                  description={this.props.startButtonDescription}
                                  action={this.props.startButtonAction}
                                  register={this.registerStartButton}
                                  enable="true"/>;

        var stopButton = <Button class="btn btn-danger center-block" id="terminate"
                                 description="Stop"
                                 register={this.registerStopButton}
                                 action={this.props.stopButtonAction}
                                 enable="true"/>;

        return (
            <div>
                <div className="container">
                    <div className="page-header text-center">
                        <h1>Welcome to the best video chat ever!</h1>
                    </div>
                    <div className="row">
                        <TagsComponent sendAction={this.props.sendTagInfoRequest}
                                       register={this.registerTags}/>
                    </div>
                    <div className="row">
                        <div className="col-md-7">
                            <div id="videoBig">
                                <video id="videoOutput" autoPlay width="640px" height="480px"
                                       poster="img/ship.png"/>
                                <div className={this.state.wait ? 'loader' : 'hidden'}/>
                            </div>
                            <div id="videoSmall">
                                <video id="videoInput" autoPlay width="240px" height="180px"
                                       poster="img/ship.png"/>
                            </div>
                        </div>
                        <div className="col-md-5">
                            <div className="row">
                                <label className="control-label" htmlFor="console">Place for tags and chat</label>
                                <Chat register={this.registerChat}
                                      enable={false}
                                      chatButtonAction={this.props.chatButtonAction}/>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12 center-block">
                            {/*{checkBox}*/}
                            {startButton}
                            {stopButton}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}