import Participant from "./Participant";
import Button from "./Button"
import Chat from "./ChatComponent";

const React = require('react');

export default class Room extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            ws: props.websocket,
            chat: null,
            id: props.id,
            name: props.name,
            participants: props.participants,
            currentHolder: props.currentHolder,
            streams: [],
            waiters: []
        };
        this.registerChat = this.registerChat.bind(this);
        this.addIceCandidateToUser = this.addIceCandidateToUser.bind(this);
        this.deleteParticipant = this.deleteParticipant.bind(this);
        this.receiveVideoAnswerForUser = this.receiveVideoAnswerForUser.bind(this);
        this.newJoinerArrived = this.newJoinerArrived.bind(this);
        this.registerParticipant = this.registerParticipant.bind(this);
        this.initOtherParticipants = this.initOtherParticipants.bind(this);
    }

    componentDidMount() {
        console.log("Created the room - " + this.state.name);
        this.props.register(this);
    }

    registerChat(object) {
        this.setState({chat: object});
    }

    registerParticipant(object) {
        if (object.state.isStreamer) {
            let currentStreams = this.state.streams;
            let newArray = [...currentStreams, object];
            this.setState({streams: newArray});
        } else {
            let currentWaiters = this.state.waiters;
            let newArray = [...currentWaiters, object];
            this.setState({waiters: newArray});
        }
    }

    addIceCandidateToUser(sessionId, candidate) {
        this.state.streams.forEach(streamer => {
            if (streamer.state.sessionId === sessionId) {
                streamer.receiveIceCandidate(candidate);
            }
        });
    }

    deleteParticipant(sessionId) {
        let streams = this.state.streams;
        let participants = this.state.participants;
        let waiters = this.state.waiters;

        streams.forEach(streamer => {
            if (streamer.state.sessionId === sessionId) {
                streamer.dispose();
            }
        });
        let participantsLeft = participants.filter(function (value, index, array) {
            return value.sessionId !== sessionId;
        });

        let streamsLeft = streams.filter(function (value, index, array) {
            return value.state.sessionId !== sessionId;
        });

        let waitersLeft = waiters.filter(function (value, index, array) {
            return value.state.sessionId !== sessionId;
        });
        let chatParticipant = {id: sessionId};
        this.state.chat.deleteParticipant(chatParticipant);
        this.setState({
            participants: participantsLeft,
            streams: streamsLeft,
            waiters: waitersLeft
        })
    }

    receiveVideoAnswerForUser(sessionId, sdpAnswer) {
        this.state.streams.forEach(streamer => {
            if (streamer.state.sessionId === sessionId) {
                streamer.receiveSdpAnswer(sdpAnswer);
            }
        });
    }

    initOtherParticipants(newParticipants) {
        let participants = this.state.participants;
        let newArray = participants;
        let chatParticipants = [];
        newParticipants.forEach(participant => {
            let newParticipant = {
                sessionId: participant.sessionId,
                name: participant.name,
                isStreamer: participant.isStreamer
            };
            newArray = [...participants, newParticipant];
            let chatParticipant = {
                id: newParticipant.sessionId,
                name: newParticipant.name
            };
            this.state.chat.addParticipant(chatParticipant);
        });
        this.setState({participants: newArray});
    }

    newJoinerArrived(newParticipant) {
        let chatParticipant = {
            id: newParticipant.sessionId,
            name: newParticipant.name
        };
        this.state.chat.addParticipant(chatParticipant);
        let participants = this.state.participants;
        const newArray = [...participants, newParticipant];
        this.setState({participants: newArray});
    }

    render() {
        let iter = 0;
        let participantList = this.state.participants.map(item => {
            let isMe = false;
            if (item.isMe) {
                isMe = true;
            }
            iter++;
            return <Participant key={iter} sessionId={item.sessionId} name={item.name}
                                isMe={isMe} websocket={this.state.ws} isStreamer={item.isStreamer}
                                register={this.registerParticipant} roomId={this.state.id}/>
        });
        return (
            <div className="container">
                <span>{this.state.name}</span>
                <div className="participants">
                    {participantList}
                </div>
                <Chat register={this.registerChat}
                      chatButtonAction={this.props.chatButtonAction}
                      enable={true}
                      chatHolder={this.state.currentHolder}
                />
                <div className="row">
                    <Button class="btn btn-danger center-block" id="leaveRoom"
                            description="Leave room"
                            action={this.props.onLeaveAction}
                            enable="true"/>
                </div>
            </div>
        )
    }
}