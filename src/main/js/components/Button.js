const React = require('react');

export default class Button extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id : props.id,
            class : props.class,
            description : props.description,
            bindAction : props.action,
            register : props.register,
            enable : props.enable,
            image: props.image
        };
        this.action = this.action.bind(this);
    }

    componentDidMount() {
        if (this.state.register) {
            this.state.register(this);
        }
    }

    action() {
        this.state.bindAction(this);
    }

    render() {
        var image;
        if (this.state.image) {
            image = <img src={this.state.image}/>
        }
        return (
            <button id={this.state.id} type="button"
                    className={this.state.class}
                    onClick={this.action}
                    disabled={!this.state.enable}>{this.state.description}
                {image}
            </button>
        )
    }
}
